/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useMemo, useContext } from "react";
import {
  Platform,
  StatusBar,
  StyleSheet,
  View,
  SafeAreaView
} from "react-native";
import { Provider } from "react-redux";
import SplashScreen from "react-native-splash-screen";
import AppNavigator from "./src/navigation/AppNavigator";
import configureStore from "./src/store";
import { colors } from "./src/utils/view";
import { UiContext, UiContextProvider } from "./src/context/ui";
import Axios from "axios";
import FlashMessage from "react-native-flash-message";

// eslint-disable-next-line no-console
console.disableYellowBox = true;

const responseLogger = response => {
  // console.log("Response:", response.config.url, response);
  return response;
};

const errorLogger = error => {
  // console.log("ERROR: ", error.response.config.url, error.response);
  // Do something with request error
  return Promise.reject(error);
};

Axios.interceptors.response.use(responseLogger, errorLogger);

function App() {
  const { isSafePink } = useContext(UiContext);
  const store = useMemo(() => configureStore({}), []);
  useEffect(() => {
    SplashScreen.hide();
  }, [store]);

  return (
    <Provider store={store}>
      {Platform.OS === "ios" && (
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle={isSafePink ? "light-content" : "dark-content"}
        />
      )}
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: isSafePink ? colors.wineBerry : colors.white
        }}
      >
        <View style={styles.view}>
          <AppNavigator />
        </View>
      </SafeAreaView>
      <FlashMessage position="top" />
    </Provider>
  );
}

function AppProvider() {
  return (
    <UiContextProvider>
      <App />
    </UiContextProvider>
  );
}

export default AppProvider;
const styles = StyleSheet.create({
  view: {
    flex: 1
  }
});
