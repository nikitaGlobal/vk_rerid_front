import React, { useEffect, useState, useMemo } from "react";
import { RootNavigator } from "./navigation";
import {AsyncStorage} from "react-native";
import { getUserData } from "../actions/user";
import { useDispatch } from "react-redux";

function App() {
  const dispatch = useDispatch();
  const [signedIn, setSignedIn] = useState(false);
  const [checkedSignIn, setCheckedSignIn] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        const token = await AsyncStorage.getItem("token");
        if (token && token === "guest") {
          setSignedIn(true);
        } else if (token) {
          await dispatch(getUserData());

          setSignedIn(true);
        } else {
          setSignedIn(false);
        }
      } catch (error) {
        AsyncStorage.clear();
        setSignedIn(false);
      } finally {
        setCheckedSignIn(true);
      }
    })();
  }, [dispatch]);

  const Layout = useMemo(() => RootNavigator(signedIn), [signedIn]);

  if (checkedSignIn) {
    return <Layout />;
  }
  return null;
}

export default App;
