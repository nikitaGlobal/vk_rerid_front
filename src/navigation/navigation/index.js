/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
import React from "react";
import { Image } from "react-native";
import {
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer
} from "react-navigation";
import { createBottomTabNavigator } from "react-navigation-tabs";
import Header, { ProfileHeader, PubHeader } from "../../components/Header";
import Launch from "../../screens/Launch/LaunchScreen";
import Login from "../../screens/Login/LoginScreen";
import Register from "../../screens/Register/RegisterScreen";
import Forgot from "../../screens/Forgot/ForgotScreen";
import Favorite from "../../screens/Favorite/FavoriteScreen";
import Popular from "../../screens/Popular/PopularScreen";
import PopularItem from "../../screens/PopularItem/ItemScreen";
import Profile from "../../screens/Profile/ProfileScreen";
import EditProfile from "../../screens/EditProfile/EditProfileScreen";
import MyJournal from "../../screens/MyJournal/MyJournalScreen";
import Journal from "../../screens/Journal";
import ChangePassword from "../../screens/ChangePassword/ChangePasswordScreen";
import NewPassword from "../../screens/NewPassword/NewPasswordScreen";
import Categories from "../../screens/Categories/CategoriesScreen";
import JournalDetails from "../../screens/JournalDetails/JournalDetailsScreen";
import Users from "../../screens/Users/UsersScreen";
import AddUser from "../../screens/AddUser/AddUserScreen";
import Articles from "../../screens/Articles/ArticlesScreen";
import ArticleDetails from "../../screens/ArticleDetails/ArticleDetailsScreen";
import Notifications from "../../screens/Notifications/NotificationsScreen";
import NotificationDetails from "../../screens/NotificationDetails/NotificationDetailsScreen";

import tab1 from "../../../assets/img/icons/tab1.png";
import tab1_active from "../../../assets/img/icons/tab1_active.png";
import tab2 from "../../../assets/img/icons/tab2.png";
import tab2_active from "../../../assets/img/icons/tab2_active.png";
import tab3 from "../../../assets/img/icons/tab3.png";
import tab3_active from "../../../assets/img/icons/tab3_active.png";
import tab4 from "../../../assets/img/icons/tab4.png";
import tab4_active from "../../../assets/img/icons/tab4_active.png";
import CreateArticle from "../../screens/CreateArticle";
import EditArticles from "../../screens/EditArticles/EditArticlesScreen";
import UserProfile from "../../screens/UserProfile/UserProfileScreen";

const publicNavigation = createStackNavigator({
  Launch: {
    screen: Launch,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  Register: {
    screen: Register,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  Forgot: {
    screen: Forgot,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  NewPassword: {
    screen: NewPassword,
    navigationOptions: {
      header: null
    }
  }
});

const FavoriteStack = createStackNavigator({
  Favorite: {
    screen: Favorite,
    navigationOptions: {
      header: null
    }
  }
});
const CategoriesStack = createStackNavigator({
  Categories: {
    screen: Categories,
    navigationOptions: {
      header: null
    }
  }
});
const ProfileStack = createStackNavigator({
  Profile: {
    screen: Profile,
    navigationOptions: {
      header: null
    }
  }
});
const MyJournalStack = createStackNavigator({
  MyJournal: {
    screen: MyJournal,
    navigationOptions: {
      header: null
    }
  }
});

const tabNavigation = createBottomTabNavigator(
  {
    Favorite: {
      screen: FavoriteStack,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Image
            source={focused ? tab1_active : tab1}
            style={{ resizeMode: "contain", width: 29, height: 29 }}
          />
        )
      }
    },
    Categories: {
      screen: CategoriesStack,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Image
            source={focused ? tab2_active : tab2}
            style={{ resizeMode: "contain", width: 29, height: 29 }}
          />
        )
      }
    },
    Profile: {
      screen: ProfileStack,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Image
            source={focused ? tab3_active : tab3}
            style={{ resizeMode: "contain", width: 29, height: 29 }}
          />
        )
      }
    },
    MyJournal: {
      screen: MyJournalStack,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Image
            source={focused ? tab4_active : tab4}
            style={{ resizeMode: "contain", width: 29, height: 29 }}
          />
        )
      }
    }
  },
  {
    initialRouteName: "Favorite",
    backBehavior: "Favorite",
    resetOnBlur: true,
    tabBarOptions: {
      showLabel: false,
      showIcon: true,
      safeAreaInset: { bottom: "never", top: "never" }
    }
  }
);

const privateNavigation = createStackNavigator({
  Home: {
    screen: tabNavigation,
    navigationOptions: ({ navigation }) => {
      const { index } = navigation.state;
      switch (index) {
        case 0:
          return {
            header: <Header navigation={navigation} title="Мои подписки" />
          };
        case 1:
          return {
            header: <Header navigation={navigation} title="Категории" />
          };
        case 2:
          return {
            header: <ProfileHeader navigation={navigation} title="Профиль" />
          };
        case 3:
          return {
            header: <Header navigation={navigation} title="Мои издания" />
          };
      }
    }
  },
  Popular: {
    screen: Popular,
    navigationOptions: ({ navigation }) => ({
      header: (
        <Header
          navigation={navigation}
          title={navigation.state.params.title}
          back
        />
      )
    })
  },
  PopularItem: {
    screen: PopularItem,
    navigationOptions: ({ navigation }) => ({
      header: (
        <Header
          navigation={navigation}
          back
          title={navigation.state.params.title}
        />
      )
    })
  },
  EditProfile: {
    screen: EditProfile,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  ChangePassword: {
    screen: ChangePassword,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  Notifications: {
    screen: Notifications,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} title="Уведомления" back />
    })
  },
  NotificationDetails: {
    screen: NotificationDetails,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  Journal: {
    screen: Journal,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  JournalDetails: {
    screen: JournalDetails,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  Users: {
    screen: Users,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  AddUser: {
    screen: AddUser,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  Articles: {
    screen: Articles,
    navigationOptions: ({ navigation }) => ({
      header: <PubHeader navigation={navigation} />
    })
  },
  CreateArticle: {
    screen: CreateArticle,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  EditArticles: {
    screen: EditArticles,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  ArticleDetails: {
    screen: ArticleDetails,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  },
  UserProfile: {
    screen: UserProfile,
    navigationOptions: ({ navigation }) => ({
      header: <Header navigation={navigation} back />
    })
  }
});

// eslint-disable-next-line import/prefer-default-export
export const RootNavigator = (signedIn = false, navigation = {}) => {
  const switchNavigator = createSwitchNavigator(
    {
      PrivateNavigation: {
        screen: privateNavigation,
        navigationOptions: {
          gesturesEnabed: false,
          navigation
        }
      },
      PublicNavigation: {
        screen: publicNavigation,
        navigationOptions: {
          gesturesEnabed: false,
          navigation
        }
      }
    },
    {
      initialRouteName: signedIn ? "PrivateNavigation" : "PublicNavigation"
    }
  );

  return createAppContainer(switchNavigator);
};
