import * as actionTypes from "../../constants/article";
import { REMOVE_ERROR_MESSAGES } from "../../constants/error";
import { LOG_OUT } from "../../constants/auth";

const initialState = {
  isLoading: false,
  error: {},
  articles: {},
  article: {},
  update: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ARTICLE_LOADING:
      return Object.assign({}, state, { isLoading: action.payload });
    case actionTypes.ARTICLE_ERROR:
      return Object.assign({}, state, { error: action.payload });
    case actionTypes.GET_ALL_ARTICLES: {
      const { articles } = state;
      const { type = "all", data = [] } = action.payload;

      articles[type] = data;
      return { ...state, articles: { ...articles } };
    }
    case actionTypes.GET_ARTICLE:
    case actionTypes.CREATE_ARTICLE:
      return { ...state, article: action.payload };
    case LOG_OUT:
      return Object.assign({}, state, initialState);
    case actionTypes.UPDATE_ARTICLE:
      return { ...state, update: true };
    case REMOVE_ERROR_MESSAGES:
      return Object.assign({}, state, { error: {}, confirmError: {} });
    default:
      return state;
  }
};
