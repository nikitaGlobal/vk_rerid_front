import * as actionTypes from "../../constants/favorite";
import { REMOVE_ERROR_MESSAGES } from "../../constants/error";
import { LOG_OUT } from "../../constants/auth";

const initialState = {
  isLoading: false,
  error: {},
  favorite: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FAVORITE_LOADING: {
      return { ...state, isLoading: action.payload };
    }
    case actionTypes.FAVORITE_ERROR: {
      return { ...state, error: action.payload };
    }
    case actionTypes.SET_FAVORITE:
      return { ...state, favorite: action.payload };
    case LOG_OUT: {
      return { ...state, ...initialState };
    }
    case REMOVE_ERROR_MESSAGES: {
      return { ...state, error: {}, confirmError: {} };
    }
    default:
      return state;
  }
};
