import * as actionTypes from "../../constants/file";
import { REMOVE_ERROR_MESSAGES } from "../../constants/error";
import { LOG_OUT } from "../../constants/auth";

const initialState = {
  isLoading: false,
  file: {},
  error: {}
};
export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FILE_LOADING: {
      return Object.assign({}, state, { isLoading: action.payload });
    }
    case actionTypes.UPLOAD_SUCCESS: {
      return Object.assign({}, state, { file: action.payload });
    }
    case actionTypes.UPLOAD_ERROR: {
      return Object.assign({}, state, { error: action.payload });
    }
    case actionTypes.REMOVE_FILE: {
      return Object.assign({}, state, { file: {} });
    }
    case LOG_OUT: {
      return Object.assign({}, state, initialState);
    }
    case REMOVE_ERROR_MESSAGES: {
      return Object.assign({}, state, { error: {}, confirmError: {} });
    }
    default:
      return state;
  }
};
