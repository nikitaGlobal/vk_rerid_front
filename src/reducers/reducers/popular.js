import * as actionTypes from "../../constants/popular";
import { REMOVE_ERROR_MESSAGES } from "../../constants/error";
import { LOG_OUT } from "../../constants/auth";
import { INCREASE_COMMENTS } from "../../constants/article";

const initialState = {
  isLoading: false,
  error: {},
  populars: [],
  popular: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.POPULAR_LOADING: {
      return { ...state, isLoading: action.payload };
    }
    case actionTypes.POPULAR_ERROR: {
      return { ...state, error: action.payload };
    }
    case actionTypes.POPULARS_DATA_SUCCESS:
      return { ...state, populars: action.payload };
    case actionTypes.POPULAR_DATA_SUCCESS:
      return { ...state, popular: action.payload };
    case LOG_OUT: {
      return { ...state, ...initialState };
    }
    case actionTypes.SET_FEEDBACK: {
      const {
        articleId,
        userId,
        like,
        dislike,
        currentAction,
        isMyAction = false
      } = action.payload;
      const { popular } = state;

      const articleIndex = popular.articles.findIndex(
        article => article.id === articleId
      );

      if (articleIndex > -1) {
        let prevAction = popular.articles[articleIndex].feedbacks.find(
          feedback => feedback.userId === userId
        );

        popular.articles[articleIndex].feedbacks = popular.articles[
          articleIndex
        ].feedbacks.filter(feedback => feedback.userId !== userId);

        popular.articles[articleIndex].feedbacks.push({
          userId,
          like,
          dislike
        });

        if (!isMyAction && prevAction) {
          if (prevAction.like) {
            popular.articles[articleIndex].like -= 1;
          } else {
            popular.articles[articleIndex].dislike -= 1;
          }
        }

        if (like) {
          if (currentAction) {
            popular.articles[articleIndex].dislike -= 1;
          }
          popular.articles[articleIndex].like += 1;
        } else {
          if (currentAction) {
            popular.articles[articleIndex].like -= 1;
          }
          popular.articles[articleIndex].dislike += 1;
        }
      }

      return { ...state, popular: { ...popular } };
    }
    case INCREASE_COMMENTS: {
      const data = { ...state.popular };
      const articleIndex = state.popular?.articles.findIndex(
        item => item.id === action.payload
      );

      if (articleIndex > -1) {
        state.popular.articles[articleIndex].commentsCount += 1;
      }
      return { ...state, popular: data };
    }
    case REMOVE_ERROR_MESSAGES: {
      return { ...state, error: {}, confirmError: {} };
    }
    default:
      return state;
  }
};
