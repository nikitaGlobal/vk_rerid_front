import * as actionTypes from "../../constants/auth";
import { REMOVE_ERROR_MESSAGES } from "../../constants/error";

const initialState = {
  isLoading: false,
  signup: false,
  user: {},
  isLoggedIn: false,
  error: {},
  passwordChanged: false
};
export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_LOADING: {
      return Object.assign({}, state, { isLoading: action.payload });
    }
    case actionTypes.USER_SIGNUP_SUCCESS: {
      return Object.assign({}, state, { signup: true });
    }
    case actionTypes.USER_SIGNUP_ERROR: {
      return Object.assign({}, state, { error: action.payload });
    }
    case actionTypes.USER_SIGNIN_SUCCESS: {
      return Object.assign({}, state, { isLoggedIn: true });
    }
    case actionTypes.USER_SIGNIN_ERROR: {
      return Object.assign({}, state, { error: action.payload });
    }
    case actionTypes.CLEAR_DATA: {
      return Object.assign({}, state, {
        signup: false,
        passwordChanged: false
      });
    }
    case actionTypes.FORGOT_ERROR: {
      return Object.assign({}, state, { error: action.payload });
    }
    case actionTypes.LOG_OUT: {
      return Object.assign({}, state, initialState);
    }
    case REMOVE_ERROR_MESSAGES: {
      return Object.assign({}, state, { error: {}, confirmError: {} });
    }
    default:
      return state;
  }
};
