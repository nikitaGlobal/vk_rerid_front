import * as actionTypes from "../../constants/edition";
import { REMOVE_ERROR_MESSAGES } from "../../constants/error";
import { LOG_OUT } from "../../constants/auth";
// import roles, { JOURNALIST } from "../../utils/editionRoles";

const initialState = {
  isLoading: false,
  error: {},
  editions: [],
  edition: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.EDITION_LOADING: {
      return { ...state, isLoading: action.payload };
    }
    case actionTypes.EDITION_ERROR: {
      return { ...state, error: action.payload };
    }
    case actionTypes.GET_ALL_EDITIONS:
      return { ...state, editions: action.payload };
    case actionTypes.GET_EDITION:
      return { ...state, edition: action.payload };
    case actionTypes.REMOVE_EDITION:
      return { ...state, edition: {} };
    case LOG_OUT: {
      return Object.assign({}, state, initialState);
    }
    case REMOVE_ERROR_MESSAGES: {
      return Object.assign({}, state, { error: {}, confirmError: {} });
    }
    default:
      return state;
  }
};
