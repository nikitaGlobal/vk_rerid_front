import * as actionTypes from "../../constants/user";
import { REMOVE_ERROR_MESSAGES } from "../../constants/error";
import { LOG_OUT } from "../../constants/auth";

const initialState = {
  isLoading: false,
  user: {},
  error: {},
  users: [],
  userById: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USER_LOADING: {
      return Object.assign({}, state, { isLoading: action.payload });
    }
    case actionTypes.USER_DATA_SUCCESS: {
      return Object.assign({}, state, { user: action.payload });
    }
    case actionTypes.USER_ERROR: {
      return Object.assign({}, state, { error: action.payload });
    }
    case actionTypes.GET_ALL_USERS:
      return { ...state, users: action.payload };
    case actionTypes.SET_USER_BY_ID: {
      return { ...state, userById: action.payload };
    }
    case LOG_OUT: {
      return Object.assign({}, state, initialState);
    }
    case REMOVE_ERROR_MESSAGES: {
      return Object.assign({}, state, { error: {}, confirmError: {} });
    }
    default:
      return state;
  }
};
