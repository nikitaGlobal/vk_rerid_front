import * as actionTypes from "../../constants/notification";
import { REMOVE_ERROR_MESSAGES } from "../../constants/error";

const initialState = {
  isLoading: false,
  error: {},
  notifications: [],
  notification: {},
  viewedCount: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.NOTIFICATION_LOADING: {
      return Object.assign({}, state, { isLoading: action.payload });
    }
    case actionTypes.NOTIFICATION_ERROR: {
      return Object.assign({}, state, { error: action.payload });
    }
    case actionTypes.GET_ALL_NOTIFICATIONS:
      return { ...state, notifications: action.payload };
    case actionTypes.GET_NOTIFICATION:
      return { ...state, notification: action.payload };
    case actionTypes.SEND_ANSWER: {
      const { notification } = state;

      notification.answers.push(action.payload);
      return { ...state, notification: { ...notification } };
    }
    case actionTypes.UPDATE_PUBLITION_STATE:
      return {
        ...state,
        notification: {
          ...state.notification,
          send: {
            ...state.notification.send,
            owner: {
              ...state.notification.send.owner,
              state: action.payload
            }
          },
          info: {
            ...state.notification.info,
            state: action.payload
          }
        }
      };
    case "SET_VIEWED_COUNT":
      return { ...state, viewedCount: action.payload };
    case REMOVE_ERROR_MESSAGES: {
      return Object.assign({}, state, { error: {}, confirmError: {} });
    }
    default:
      return state;
  }
};
