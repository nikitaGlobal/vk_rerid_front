import * as actionTypes from "../../constants/category";
import { REMOVE_ERROR_MESSAGES } from "../../constants/error";
import { LOG_OUT } from "../../constants/auth";

const initialState = {
  isLoading: false,
  error: {},
  categories: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CATEGORY_LOADING: {
      return Object.assign({}, state, { isLoading: action.payload });
    }
    case actionTypes.CATEGORY_ERROR: {
      return Object.assign({}, state, { error: action.payload });
    }
    case actionTypes.GET_ALL_CATEGORIES:
      return { ...state, categories: action.payload };
    case LOG_OUT: {
      return Object.assign({}, state, initialState);
    }
    case REMOVE_ERROR_MESSAGES: {
      return Object.assign({}, state, { error: {}, confirmError: {} });
    }
    default:
      return state;
  }
};
