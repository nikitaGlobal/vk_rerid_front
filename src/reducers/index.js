import { combineReducers } from "redux";

import notification from "./reducers/notification";
import category from "./reducers/category";
import favorite from "./reducers/favorite";
import edition from "./reducers/edition";
import popular from "./reducers/popular";
import article from "./reducers/article";
import auth from "./reducers/auth";
import user from "./reducers/user";
import file from "./reducers/file";

export default function getRootReducer(navReducer) {
  return combineReducers({
    nav: navReducer,
    notification,
    category,
    favorite,
    edition,
    popular,
    article,
    auth,
    user,
    file
  });
}
