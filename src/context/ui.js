import React, { createContext, useState } from "react";

const UiContext = createContext();

function UiContextProvider({ children }) {
  const [isSafePink, setSafePink] = useState(false);
  const value = { isSafePink, setSafePink };
  return <UiContext.Provider value={value}>{children}</UiContext.Provider>;
}

export { UiContext, UiContextProvider };
