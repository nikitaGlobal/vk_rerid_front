import axios from "axios";
import {AsyncStorage} from "react-native";
import api from "../../api";
import * as actionTypes from "../constants/favorite";
import createAction from "../utils/creatAction";

const getAllFavorites = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}favorite`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      return dispatch(createAction(actionTypes.SET_FAVORITE, res.data.data));
    } catch (err) {
      dispatch(createAction(actionTypes.FAVORITE_ERROR, err.response.data));
    }
  }
};
const addOrRemoveFavorite = data => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}favorite`,
        method: "POST",
        data,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });
      dispatch(getAllFavorites());
      return res.data.data;
    } catch (err) {
      dispatch(createAction(actionTypes.POPULAR_ERROR, err.response.data));
    }
  }
};
export { getAllFavorites, addOrRemoveFavorite };
