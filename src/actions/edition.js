import axios from "axios";
import {AsyncStorage} from "react-native";
import api from "../../api";
import * as actionTypes from "../constants/edition";
import createAction from "../utils/creatAction";
import roles from "../utils/editionRoles";

const getAllEditions = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    dispatch(createAction(actionTypes.EDITION_LOADING, true));
    try {
      const res = await axios({
        url: `${api.url}publications/editions`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      const data = [];
      res.data.data.forEach(item => {
        if (item.pubId) {
          const roleIndex = data.findIndex(elem => elem.role === item.role);

          if (roleIndex > -1) {
            data[roleIndex].data.push(item);
          } else {
            data.push({
              title: roles[item.role],
              role: item.role,
              data: [item]
            });
          }
        }
      });

      dispatch(createAction(actionTypes.GET_ALL_EDITIONS, data));
    } catch (err) {
      dispatch(createAction(actionTypes.EDITION_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.EDITION_LOADING, false));
    }
  }
};

const getEdition = id => async dispatch => {
  dispatch(createAction(actionTypes.EDITION_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}publications/id/${id}`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });
      dispatch(createAction(actionTypes.GET_EDITION, res.data.data));
      return res.data.data;
    } catch (err) {
      dispatch(createAction(actionTypes.EDITION_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.EDITION_LOADING, false));
    }
  }
};

const createEdition = data => async dispatch => {
  dispatch(createAction(actionTypes.EDITION_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}publications/create`,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        data
      });

      dispatch(getAllEditions());
      dispatch(createAction(actionTypes.GET_EDITION, res.data.data));
      return;
    } catch (err) {
      dispatch(createAction(actionTypes.EDITION_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.EDITION_LOADING, false));
    }
  }
};

const updateEdition = data => async dispatch => {
  dispatch(createAction(actionTypes.EDITION_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}publications/update`,
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        data
      });

      dispatch(getAllEditions());
      return res.data.data;
    } catch (err) {
      dispatch(createAction(actionTypes.EDITION_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.EDITION_LOADING, false));
    }
  }
};

const inviteUser = data => async dispatch => {
  dispatch(createAction(actionTypes.EDITION_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}publications/invite-user`,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        data
      });

      return res.data.data;
    } catch (err) {
      dispatch(createAction(actionTypes.EDITION_ERROR, err.response.data));
      return err.response.data;
    } finally {
      dispatch(createAction(actionTypes.EDITION_LOADING, false));
    }
  }
};

const removeEdition = () => async dispatch => {
  dispatch(createAction(actionTypes.REMOVE_EDITION));
};

export {
  getAllEditions,
  createEdition,
  removeEdition,
  inviteUser,
  getEdition,
  updateEdition
};
