import axios from "axios";
import {AsyncStorage} from "react-native";
import api from "../../api";
import * as actionTypes from "../constants/publications";
import createAction from "../utils/creatAction";

const getPopular = id => async dispatch => {
  dispatch(createAction(actionTypes.PUB_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}publications/popular/${id}`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      dispatch(createAction(actionTypes.CHANGE_VISIBLE, res.data.data));
    } catch (err) {
      dispatch(createAction(actionTypes.PUB_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.PUB_LOADING, false));
    }
  }
};

export { getPopular };
