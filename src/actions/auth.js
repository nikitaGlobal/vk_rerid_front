import axios from "axios";
import { showMessage } from "react-native-flash-message";
import { setStorage, logOut as logOutStorage } from "../utils/auth";
import api from "../../api";
import * as actionTypes from "../constants/auth";
import createAction from "../utils/creatAction";
import { getUserData } from "./user";
import { Alert } from "react-native";

const signIn = data => async dispatch => {
  dispatch(createAction(actionTypes.AUTH_LOADING, true));
  try {
    const res = await axios({
      url: `${api.url}auth/signin`,
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      data
    });

    if (!res.data.data.isConfirmed) {
      return Alert.alert(
        "Предупреждение",
        "Пожалуйста, проверьте свою электронную почту, чтобы активировать учетную запись",
        [
          {
            text: "OK"
          }
        ]
      );
    }
    setStorage("token", res.data.data.token);
    dispatch(createAction(actionTypes.USER_SIGNIN_SUCCESS, res.data));
    dispatch(getUserData());
  } catch (err) {
    dispatch(createAction(actionTypes.USER_SIGNIN_ERROR, err.response.data));
  } finally {
    dispatch(createAction(actionTypes.AUTH_LOADING, false));
  }
};

const signUp = data => async dispatch => {
  dispatch(createAction(actionTypes.AUTH_LOADING, true));
  try {
    const res = await axios({
      url: `${api.url}auth/signup`,
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      data
    });
    dispatch(createAction(actionTypes.USER_SIGNUP_SUCCESS, res.data));
  } catch (err) {
    dispatch(createAction(actionTypes.USER_SIGNUP_ERROR, err.response.data));
  } finally {
    dispatch(createAction(actionTypes.AUTH_LOADING, false));
  }
};

const forgotPassword = data => async dispatch => {
  dispatch(createAction(actionTypes.AUTH_LOADING, true));
  try {
    await axios({
      url: `${api.url}auth/forgot`,
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      data
    });
    showMessage({
      message:
        "Запрос успешно отправлен, пожалуйста, проверьте вашу электронную почту для дальнейших инструкций",
      type: "success"
    });
  } catch (err) {
    dispatch(createAction(actionTypes.FORGOT_ERROR, err.response.data));
  } finally {
    dispatch(createAction(actionTypes.AUTH_LOADING, false));
  }
};

const saveNewPassword = (data, token) => async dispatch => {
  dispatch(createAction(actionTypes.AUTH_LOADING, true));
  try {
    await axios({
      url: `${api.url}users/forgot-pass`,
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      data
    });

    return showMessage({
      message: "Пароль успешно обновлен, пожалуйста войдите",
      type: "success"
    });
  } catch (err) {
    dispatch(createAction(actionTypes.FORGOT_ERROR, err.response.data));
  } finally {
    dispatch(createAction(actionTypes.AUTH_LOADING, false));
  }
};

const logOut = () => dispatch => {
  logOutStorage();
  dispatch(createAction(actionTypes.LOG_OUT));
};

const clearData = () => dispatch => {
  dispatch(createAction(actionTypes.CLEAR_DATA));
};

export { logOut, signIn, signUp, clearData, forgotPassword, saveNewPassword };
