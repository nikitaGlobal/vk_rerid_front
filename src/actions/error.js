import createAction from "../utils/creatAction";
import { REMOVE_ERROR_MESSAGES } from "../constants/error";

const removeErrorMessages = () => {
  return dispatch => {
    dispatch(createAction(REMOVE_ERROR_MESSAGES));
  };
};

export { removeErrorMessages };
