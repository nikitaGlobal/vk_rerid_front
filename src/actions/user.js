import axios from "axios";
import {AsyncStorage} from "react-native";
import { showMessage } from "react-native-flash-message";
import api from "../../api";
import * as actionTypes from "../constants/user";
import createAction from "../utils/creatAction";
import { REMOVE_FILE } from "../constants/file";
import { logOut } from "./auth";

const getUserData = updated => async dispatch => {
  dispatch(createAction(actionTypes.USER_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}users/me`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      if (updated === "updated") {
        showMessage({
          message: "Данные успешно обновлены",
          type: "success"
        });
      }

      if (res.data.data.user) {
        dispatch(createAction(actionTypes.USER_DATA_SUCCESS, res.data.data));
      } else {
        dispatch(logOut());
      }
      return;
    } catch (err) {
      if (err.response?.status === 401) {
        dispatch(logOut());
        // navigation.navigate("Launch");
      }
      dispatch(createAction(actionTypes.USER_ERROR, err.response.data));
      return Promise.reject(err);
    } finally {
      dispatch(createAction(actionTypes.USER_LOADING, false));
    }
  }
};

const editProfile = data => async dispatch => {
  dispatch(createAction(actionTypes.USER_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  try {
    await axios({
      url: `${api.url}users/update`,
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      data
    });

    dispatch(getUserData("updated"));
    dispatch(createAction(REMOVE_FILE));
  } catch (err) {
    dispatch(createAction(actionTypes.USER_ERROR, err.response.data));
  }
};

const editPassword = data => async dispatch => {
  dispatch(createAction(actionTypes.USER_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  try {
    axios({
      url: `${api.url}users/update-pass`,
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      data
    });

    showMessage({
      message: "Данные успешно обновлены",
      type: "success"
    });
  } catch (err) {
    dispatch(createAction(actionTypes.USER_ERROR, err.response.data));
  } finally {
    dispatch(createAction(actionTypes.USER_LOADING, false));
  }
};

const getAllUsers = () => async dispatch => {
  dispatch(createAction(actionTypes.USER_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}users`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      dispatch(createAction(actionTypes.GET_ALL_USERS, res.data.data));
    } catch (err) {
      dispatch(createAction(actionTypes.USER_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.USER_LOADING, false));
    }
  }
};

const getUserById = id => async dispatch => {
  dispatch(createAction(actionTypes.USER_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}users/id/${id}`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      dispatch(createAction(actionTypes.SET_USER_BY_ID, res.data.data));
    } catch (err) {
      dispatch(createAction(actionTypes.USER_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.USER_LOADING, false));
    }
  }
};

export { getUserData, editProfile, editPassword, getAllUsers, getUserById };
