import axios from "axios";
import { Platform, AsyncStorage } from "react-native";
import api from "../../api";
import * as actionTypes from "../constants/file";
import createAction from "../utils/creatAction";

const uploadAttachments = data => {
  return async dispatch => {
    dispatch(createAction(actionTypes.FILE_LOADING, true));
    let file = {
      name: "file",
      type: data.mime
    };
    const token = await AsyncStorage.getItem("token");
    const formData = new FormData();
    if (Platform === "ios") {
      file.uri = `file://${data.path}`;
    } else {
      file.uri = data.path;
    }
    formData.append("file", file);
    try {
      const res = await axios({
        url: `${api.url}upload/attachments/`,
        method: "POST",
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${token}`
        },
        data: formData
      });

      dispatch(createAction(actionTypes.UPLOAD_SUCCESS, res.data.image));
      return res.data.image;
    } catch (err) {
      dispatch(createAction(actionTypes.UPLOAD_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.FILE_LOADING, false));
    }
  };
};

export { uploadAttachments };
