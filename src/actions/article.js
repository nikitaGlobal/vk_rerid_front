import axios from "axios";
import {AsyncStorage} from "react-native";
import api from "../../api";
import * as actionTypes from "../constants/article";
import createAction from "../utils/creatAction";
import { ARTICLE_DRAFT } from "../utils/editionRoles";
import { showMessage } from "react-native-flash-message";

const getAllArticles = (pubId, type) => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    dispatch(createAction(actionTypes.ARTICLE_LOADING, true));
    try {
      const res = await axios({
        url: `${api.url}article`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        params: {
          pubId,
          type
        }
      });

      return dispatch(
        createAction(actionTypes.GET_ALL_ARTICLES, {
          data: res.data.data,
          type
        })
      );
    } catch (err) {
      dispatch(createAction(actionTypes.ARTICLE_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.ARTICLE_LOADING, false));
    }
  }
};

const getDraftArticles = pubId => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    dispatch(createAction(actionTypes.ARTICLE_LOADING, true));
    try {
      const res = await axios({
        url: `${api.url}article/draft/${pubId}`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      return dispatch(
        createAction(actionTypes.GET_ALL_ARTICLES, {
          data: res.data.data,
          type: ARTICLE_DRAFT
        })
      );
    } catch (err) {
      dispatch(createAction(actionTypes.ARTICLE_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.ARTICLE_LOADING, false));
    }
  }
};

const createArticle = data => async dispatch => {
  dispatch(createAction(actionTypes.ARTICLE_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}article/create`,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        data
      });
      dispatch(createAction(actionTypes.CREATE_ARTICLE, res.data.data));
    } catch (err) {
      dispatch(createAction(actionTypes.ARTICLE_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.ARTICLE_LOADING, false));
    }
  }
};
const updateArticle = data => async dispatch => {
  dispatch(createAction(actionTypes.ARTICLE_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}article/update`,
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        data
      });

      showMessage({
        message: "статья успешно обновлено",
        type: "success"
      });

      dispatch(createAction(actionTypes.UPDATE_ARTICLE, res.data.data));
    } catch (err) {
      dispatch(createAction(actionTypes.ARTICLE_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.ARTICLE_LOADING, false));
    }
  }
};

const updateTyoeArticle = (id, pubId, type) => async dispatch => {
  dispatch(createAction(actionTypes.ARTICLE_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      await axios({
        url: `${api.url}article/update-type/${id}`,
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        data: {
          pubId,
          type
        }
      });

      dispatch(getAllArticles(pubId));
      showMessage({
        message: "статья успешно обновлено",
        type: "success"
      });
    } catch (err) {
      dispatch(createAction(actionTypes.ARTICLE_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.ARTICLE_LOADING, false));
    }
  }
};

const getArticleById = id => async dispatch => {
  dispatch(createAction(actionTypes.ARTICLE_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}article/id/${id}`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      dispatch(createAction(actionTypes.GET_ARTICLE, res.data.data));
      return res.data.data;
    } catch (err) {
      dispatch(createAction(actionTypes.ARTICLE_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.ARTICLE_LOADING, false));
    }
  }
};

const changeArticleStatus = (id, pubId) => async dispatch => {
  dispatch(createAction(actionTypes.ARTICLE_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}article/change-status`,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        data: {
          artId: id,
          pubId
        }
      });

      return res.data.data;
    } catch (err) {
      dispatch(createAction(actionTypes.ARTICLE_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.ARTICLE_LOADING, false));
    }
  }
};

export {
  getAllArticles,
  createArticle,
  getArticleById,
  changeArticleStatus,
  updateArticle,
  updateTyoeArticle,
  getDraftArticles
};
