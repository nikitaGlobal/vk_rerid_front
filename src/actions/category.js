import axios from "axios";
import {AsyncStorage} from "react-native";
import api from "../../api";
import * as actionTypes from "../constants/category";
import createAction from "../utils/creatAction";

const getAllCategories = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}categories`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      return dispatch(
        createAction(actionTypes.GET_ALL_CATEGORIES, res.data.data)
      );
    } catch (err) {
      dispatch(createAction(actionTypes.CATEGORY_ERROR, err.response.data));
    }
  }
};

export { getAllCategories };
