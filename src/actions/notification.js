import axios from "axios";
import {AsyncStorage} from "react-native";
import api from "../../api";
import * as actionTypes from "../constants/notification";
import createAction from "../utils/creatAction";
import { showMessage } from "react-native-flash-message";

const getAllNotifications = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}article/comments`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      dispatch(createAction(actionTypes.GET_ALL_NOTIFICATIONS, res.data.data));
      return res.data.data.length;
    } catch (err) {
      dispatch(createAction(actionTypes.NOTIFICATION_ERROR, err.response.data));
    }
  }
};

const getNotification = id => async dispatch => {
  dispatch(createAction(actionTypes.NOTIFICATION_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}article/comment/${id}`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      dispatch(createAction(actionTypes.GET_NOTIFICATION, res.data.data));
    } catch (err) {
      dispatch(createAction(actionTypes.NOTIFICATION_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.NOTIFICATION_LOADING, false));
    }
  }
};

const answerQuestion = data => async dispatch => {
  dispatch(createAction(actionTypes.NOTIFICATION_LOADING, true));
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}article/comment-answer`,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        data
      });

      return dispatch(createAction(actionTypes.SEND_ANSWER, res.data.data));
    } catch (err) {
      dispatch(createAction(actionTypes.NOTIFICATION_ERROR, err.response.data));
    } finally {
      dispatch(createAction(actionTypes.NOTIFICATION_LOADING, false));
    }
  }
};

const updatePublitionState = (id, state) => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      await axios({
        url: `${api.url}publications/state/${id}`,
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        data: {
          state
        }
      });

      return dispatch(createAction(actionTypes.UPDATE_PUBLITION_STATE, state));
    } catch (err) {
      showMessage({
        message: err?.response?.data?.message,
        type: "danger"
      });
      return Promise.reject(err);
    }
  }
};

export {
  getAllNotifications,
  getNotification,
  answerQuestion,
  updatePublitionState
};
