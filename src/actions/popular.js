import axios from "axios";
import {AsyncStorage} from "react-native";
import api from "../../api";
import * as actionTypes from "../constants/popular";
import createAction from "../utils/creatAction";

const getPopularByCatId = id => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}publications/popular/${id}`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });

      return dispatch(
        createAction(actionTypes.POPULARS_DATA_SUCCESS, res.data.data)
      );
    } catch (err) {
      dispatch(createAction(actionTypes.POPULAR_ERROR, err.response.data));
    }
  }
};
const getPopularById = id => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  if (token !== "guest") {
    try {
      const res = await axios({
        url: `${api.url}publications/id/${id}`,
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });
      return dispatch(
        createAction(actionTypes.POPULAR_DATA_SUCCESS, res.data.data)
      );
    } catch (err) {
      dispatch(createAction(actionTypes.POPULAR_ERROR, err.response.data));
    }
  }
};

export { getPopularByCatId, getPopularById };
