import React, { useEffect, useCallback, useContext } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  Image,
  Text,
  Linking
} from "react-native";
import Ripple from "react-native-material-ripple";
import { NavigationEvents } from "react-navigation";
import { useNavigation } from "react-navigation-hooks";
import { colors, sizes, family } from "../../utils/view";
import logo from "../../../assets/img/logo.png";
import Button from "../../components/Button";
import { setStorage } from "../../utils/auth";
import { UiContext } from "../../context/ui";

function Launch() {
  const { navigate } = useNavigation();
  const { setSafePink } = useContext(UiContext);

  const handleOpenURL = useCallback(
    event => {
      handleNavigate(event.url);
    },
    [handleNavigate]
  );

  const handleNavigate = useCallback(
    url => {
      const route = url.replace(/.*?:\/\//g, "");
      const routeName = route.split("/")[0];
      // eslint-disable-next-line no-useless-escape
      const params = route.match(/\/([^\/]+)\/?$/);
      let token;
      if (params) {
        token = params[1];
      }

      if (routeName === "forgot") {
        navigate("NewPassword", { token });
      } else if (routeName === "login") {
        navigate("Login");
      }
    },
    [navigate]
  );

  useEffect(() => {
    Linking.getInitialURL()
      .then(event => {
        if (event) {
          handleNavigate(event);
        }
      })
      .catch(() => {});
    Linking.addEventListener("url", handleOpenURL);

    return () => {
      Linking.removeEventListener("url", handleOpenURL);
    };
  }, [handleNavigate, handleOpenURL]);

  const handlePressContinue = useCallback(() => {
    setStorage("token", "guest");
    navigate("Favorite");
  }, [navigate]);

  const handleBlurNavigation = useCallback(() => {
    setSafePink(false);
  }, [setSafePink]);

  const handleFocusNavigation = useCallback(() => {
    setSafePink(true);
  }, [setSafePink]);

  return (
    <View style={styles.container}>
      <NavigationEvents
        onWillBlur={handleBlurNavigation}
        onWillFocus={handleFocusNavigation}
      />
      <ScrollView contentContainerStyle={styles.scrollCont}>
        <View style={styles.logo}>
          <Image source={logo} style={styles.image} />
          <View style={styles.reklam}>
            <Text style={styles.reklamText}>Рекламный слоган</Text>
          </View>
        </View>
        <View style={styles.guestContainer}>
          <View style={styles.alCenter}>
            <Button
              onPress={() => {
                navigate("Register");
              }}
              title="Создать учетную запись"
              container={{
                paddingHorizontal: 21,
                borderColor: colors.prim,
                borderStyle: "solid",
                borderWidth: 1.2,
                textTransform: "uppercase"
              }}
              style={{ color: colors.white }}
            />
            <Button
              onPress={() => {
                navigate("Login");
              }}
              title="Войти"
              container={{
                backgroundColor: colors.prim,
                marginTop: 24,
                paddingHorizontal: 31,
                textTransform: "uppercase"
              }}
              // eslint-disable-next-line react-native/no-inline-styles
              style={{ color: colors.wineBerry, textTransform: "uppercase" }}
            />
          </View>
        </View>
        <View style={styles.bottom}>
          <View style={styles.foot}>
            <Ripple onPress={handlePressContinue}>
              <Text style={styles.text}>Продолжить без регистрации</Text>
            </Ripple>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.wineBerry,
    padding: 24
  },
  scrollCont: {
    flex: 1
  },
  logo: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: 192,
    height: 57,
    marginBottom: 54
  },
  bottom: {
    flex: 1
  },
  foot: {
    flex: 1,
    justifyContent: "flex-end",
    paddingBottom: 23
  },
  alCenter: {
    alignItems: "center"
  },
  text: {
    color: colors.white,
    fontFamily: family.regular,
    textAlign: "center",
    // fontWeight: "500",
    textTransform: "uppercase",
    ...sizes.medium
  },
  reklamText: {
    color: colors.white,
    textAlign: "center",
    fontFamily: family.regular,
    ...sizes.input
  },
  guestContainer: {
    justifyContent: "center",
    flex: 1
  }
});

export default Launch;
