import React, { useRef, useEffect, useCallback } from "react";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { View, StyleSheet, Text } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { showMessage } from "react-native-flash-message";
import { removeErrorMessages } from "../../actions/error";
import { family, colors, sizes } from "../../utils/view";
import useHandleChange from "../../hooks/handleChange";
import { forgotPassword } from "../../actions/auth";
import Loading from "../../components/Loading";
import Button from "../../components/Button";
import Input from "../../components/Input";

function Forgot() {
  const dispatch = useDispatch();
  const { isLoading, error } = useSelector(({ auth }) => ({
    isLoading: auth.isLoading,
    error: auth.error
  }));
  const scroll = useRef(null);

  const email = useHandleChange("");

  useEffect(() => {
    if (!_.isEmpty(error)) {
      showMessage({
        message: error.message,
        type: "danger"
      });
      dispatch(removeErrorMessages());
    }
  }, [dispatch, error]);

  const handleSubmit = useCallback(() => {
    const isValid = email.validate({ targetValue: email.value, type: "email" });

    if (isValid) {
      dispatch(forgotPassword({ email: email.value }));
    }
  }, [dispatch, email]);

  const scrollToInput = useCallback(reactNode => {
    scroll.current.scrollToFocusedInput(reactNode);
  }, []);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollCont}
        showsVerticalScrollIndicator={false}
        style={styles.scroll}
        keyboardDismissMode="none"
        keyboardShouldPersistTaps="handled"
        ref={scroll}
      >
        <View style={styles.content}>
          <Text style={styles.title}>Восстановление пароля</Text>
          <Text style={styles.advice}>
            Для восстановления пароля введите email, указанный при регистрации в
            приложении ReRider.
          </Text>
        </View>
        <View>
          <Input
            scrollToInput={scrollToInput}
            label="E-mail"
            blurOnSubmit={false}
            returnKeyType="next"
            autoCapitalize="none"
            keyboardType="email-address"
            onSubmitEditing={handleSubmit}
            value={email.value}
            onChangeText={text =>
              email.onChange({
                targetValue: text,
                type: "email"
              })
            }
            error={email.errorMessage}
          />
        </View>
        <View style={styles.foot}>
          <Button
            onPress={handleSubmit}
            title="Отправить"
            container={{ backgroundColor: colors.tapestry, width: 162 }}
            style={{ color: colors.white }}
          />
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  scrollCont: {
    flex: 1
  },
  scroll: {
    paddingHorizontal: 19,
    paddingBottom: 19
  },
  title: {
    fontFamily: family.medium,
    ...sizes.title,
    color: colors.wineBerry,
    textAlign: "center"
  },
  advice: {
    marginTop: 33,
    ...sizes.subTitle,
    fontFamily: family.regular,
    color: colors.tundora
  },
  content: {
    paddingBottom: 24
  },
  foot: {
    alignItems: "center",
    paddingTop: 40
  }
});

export default Forgot;
