import React, { useRef, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { View, StyleSheet } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import _ from "lodash";
import { showMessage } from "react-native-flash-message";
import Input from "../../components/Input";
import { colors } from "../../utils/view";
import Button from "../../components/Button";
import Loading from "../../components/Loading";
import useHandleChange from "../../hooks/handleChange";
import { removeErrorMessages } from "../../actions/error";
import { saveNewPassword } from "../../actions/auth";

function ChangePassword({ navigation }) {
  const dispatch = useDispatch();
  const { error, isLoading } = useSelector(({ auth }) => ({
    isLoading: auth.isLoading,
    error: auth.error
  }));
  const scroll = useRef(null);
  const passwordRef = useRef(null);
  const confirmPasswordRef = useRef(null);

  const password = useHandleChange("");
  const confirmPassword = useHandleChange("");

  useEffect(() => {
    if (!_.isEmpty(error)) {
      showMessage({
        message: error.message,
        type: "danger"
      });
      dispatch(removeErrorMessages());
    }
  }, [dispatch, error]);

  const validateField = useCallback(
    ({ filed, type, length, secondValue, errorMessage }) =>
      filed.validate({
        targetValue: filed.value,
        type,
        length,
        secondValue,
        errorMessage
      }),
    []
  );

  const handleSubmit = useCallback(() => {
    const isValid =
      validateField({ filed: password, type: "password", length: 8 }) &&
      validateField({
        filed: confirmPassword,
        type: "equal",
        secondValue: password.value,
        errMessage: "Пароли не совпадают"
      });
    if (isValid) {
      dispatch(
        saveNewPassword(
          { password: password.value },
          navigation.getParam("token"),
          navigation
        )
      ).then(() => {
        navigation.navigate("Login");
      });
    }
  }, [confirmPassword, dispatch, navigation, password, validateField]);

  const scrollToInput = useCallback(reactNode => {
    scroll.current.scrollToFocusedInput(reactNode);
  }, []);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.content}
        keyboardDismissMode="none"
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        ref={scroll}
      >
        <View style={styles.content}>
          <Input
            scrollToInput={scrollToInput}
            label="Введите новый пароль"
            blurOnSubmit={false}
            secureTextEntry={true}
            returnKeyType="next"
            autoCapitalize="none"
            onSubmitEditing={() => confirmPasswordRef.current.focus()}
            inputRef={passwordRef}
            value={password.value}
            onChangeText={text => {
              password.onChange({
                targetValue: text,
                type: "password"
              });
            }}
            error={password.errorMessage}
          />
          <Input
            scrollToInput={scrollToInput}
            label="Введите новый пароль повторно"
            blurOnSubmit={true}
            secureTextEntry={true}
            returnKeyType="done"
            autoCapitalize="none"
            onSubmitEditing={handleSubmit}
            inputRef={confirmPasswordRef}
            value={confirmPassword.value}
            onChangeText={text =>
              confirmPassword.onChange({
                targetValue: text,
                type: "equal",
                secondValue: password.value,
                errMessage: "Пароли не совпадают"
              })
            }
            error={confirmPassword.errorMessage}
          />
        </View>
        <View style={styles.footer}>
          <Button
            onPress={handleSubmit}
            title="Отправить"
            container={{ backgroundColor: colors.tapestry }}
            style={{ color: colors.white }}
          />
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 19
  },
  content: {
    flex: 1
  },
  footer: {
    paddingTop: 20
  }
});

export default ChangePassword;
