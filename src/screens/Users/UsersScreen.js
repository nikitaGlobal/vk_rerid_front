/* eslint-disable react-native/no-raw-text */
import React, { useCallback, useEffect, useRef, memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { StyleSheet, FlatList, View, Image } from "react-native";
import Ripple from "react-native-material-ripple";
import Menu, { MenuItem } from "react-native-material-menu";
import more from "../../../assets/img/more.png";
import { colors } from "../../utils/view";
import ListItemWithMenu from "../../components/ListItemWithMenu";
import { getAllUsers } from "../../actions/user";
import Loading from "../../components/Loading";
import { useNavigation, useNavigationParam } from "react-navigation-hooks";

function User({ name, avatar, id }) {
  const { navigate } = useNavigation();
  const pubId = useNavigationParam("pubId");

  const menuRef = useRef(null);

  const handleInvite = useCallback(() => {
    navigate("AddUser", {
      userId: id,
      pubId: pubId
    });
    menuRef.current.hide();
  }, [id, navigate, pubId]);

  const handleViewProfile = useCallback(() => {
    menuRef.current.hide();
    navigate("UserProfile", { id, pubId: pubId });
  }, [id, navigate, pubId]);

  const showMenu = useCallback(() => {
    menuRef.current.show();
  }, []);

  return (
    <ListItemWithMenu
      name={name}
      image={avatar}
      id={id}
      onPress={handleViewProfile}
    >
      <Menu
        ref={menuRef}
        style={styles.menu}
        button={
          <Ripple style={styles.btn} onPress={showMenu}>
            <Image source={more} style={styles.settings} />
          </Ripple>
        }
      >
        <MenuItem onPress={handleInvite}>Пригласить</MenuItem>
        <MenuItem onPress={handleViewProfile}>См. Профиль</MenuItem>
      </Menu>
    </ListItemWithMenu>
  );
}

User.defaultProps = {
  image: "",
  role: "",
  children: null
};

const UserMemo = memo(User);

function Users() {
  const dispatch = useDispatch();
  const { users, isLoading } = useSelector(({ user }) => ({
    users: user.users,
    isLoading: user.isLoading
  }));
  useEffect(() => {
    dispatch(getAllUsers());
  }, [dispatch]);

  const renderItem = useCallback(
    ({ item }) => (
      <UserMemo
        name={item.name}
        image={item.avatar}
        role={item.role}
        id={item.id}
      />
    ),
    []
  );

  const keyExtractor = useCallback(item => item.id, []);

  if (isLoading) {
    return <Loading />;
  }
  return (
    <View style={styles.root}>
      <FlatList
        data={users}
        style={styles.container}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingLeft: 20,
    paddingRight: 20
  },
  settings: {
    resizeMode: "contain",
    width: 29,
    height: 29
  },
  btn: {
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden"
  },
  menu: {
    marginTop: 40
  }
});

export default Users;
