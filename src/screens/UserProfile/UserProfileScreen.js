import React, { useEffect, useMemo, useCallback } from "react";
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  SectionList
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { colors, sizes, family } from "../../utils/view";
import Loading from "../../components/Loading";
import { Img } from "../../components/Custom";
import { getUserById } from "../../actions/user";
import { useNavigationParam, useNavigation } from "react-navigation-hooks";
import { PubItems } from "../../components/Publications";
import {
  EDITOR,
  JOURNALIST,
  CORRECTOR,
  LAWYER
} from "../../utils/editionRoles";
import Button from "../../components/Button";

function UserProfile() {
  const id = useNavigationParam("id");
  const pubId = useNavigationParam("pubId");

  const { navigate } = useNavigation();
  const dispatch = useDispatch();
  const { isLoading, userById } = useSelector(({ user }) => ({
    isLoading: user.isLoading,
    userById: user.userById
  }));

  useEffect(() => {
    dispatch(getUserById(id));
  }, [dispatch, id]);

  const sections = useMemo(() => {
    const data = [];

    userById.pubUsers &&
      userById.pubUsers.forEach(item => {
        switch (item.role) {
          case EDITOR: {
            const index = data.findIndex(item => item.role === EDITOR);

            if (index > -1) {
              data[index].data.push(item);
            } else {
              data.push({
                title: "Редактор в издательствах",
                role: EDITOR,
                data: [item]
              });
            }
            break;
          }
          case JOURNALIST: {
            const index = data.findIndex(item => item.role === JOURNALIST);

            if (index > -1) {
              data[index].data.push(item);
            } else {
              data.push({
                title: "Журналист в издательствах",
                role: JOURNALIST,
                data: [item]
              });
            }
            break;
          }
          case CORRECTOR: {
            const index = data.findIndex(item => item.role === CORRECTOR);

            if (index > -1) {
              data[index].data.push(item);
            } else {
              data.push({
                title: "Корректор в издательствах",
                role: CORRECTOR,
                data: [item]
              });
            }
            break;
          }
          case LAWYER: {
            const index = data.findIndex(item => item.role === LAWYER);

            if (index > -1) {
              data[index].data.push(item);
            } else {
              data.push({
                title: "Юрист в издательствах",
                role: LAWYER,
                data: [item]
              });
            }
            break;
          }
        }
      });
    return data;
  }, [userById.pubUsers]);

  const renderItem = useCallback(
    ({ item: publication }) => {
      if (!publication.publication) return null;
      return (
        <TouchableOpacity
          onPress={() =>
            navigate("PopularItem", {
              title: publication.publication.name,
              id: publication.publication.id
            })
          }
        >
          <PubItems
            style={styles.pubWrap}
            name={publication.publication.name}
            image={publication.publication.avatar}
          />
        </TouchableOpacity>
      );
    },
    [navigate]
  );

  const handleinvite = useCallback(() => {
    navigate("AddUser", {
      userId: id,
      pubId
    });
  }, [id, navigate, pubId]);

  if (isLoading || !Object.keys(userById).length) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <ScrollView
        contentContainerStyle={styles.scrollCont}
        style={styles.scroll}
      >
        <View style={styles.row}>
          <View style={styles.imageCont}>
            <Img image={userById.avatar} style={styles.image} url />
          </View>
          <View>
            <Text style={styles.name}>{userById.name}</Text>
          </View>
        </View>
        <View style={styles.description}>
          <Text style={styles.about}>{userById.description}</Text>
        </View>
        <View style={styles.content}>
          <SectionList
            keyExtractor={item => item.id}
            sections={sections}
            renderItem={renderItem}
            renderSectionHeader={({ section: { title } }) => (
              <Text style={styles.title}>{title}</Text>
            )}
            scrollEnabled={false}
          />
        </View>
      </ScrollView>
      <View style={styles.footer}>
        <Button
          onPress={handleinvite}
          title="Пригласить"
          container={{ backgroundColor: colors.tapestry, width: "100%" }}
          style={{ color: colors.white }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  scrollCont: {
    flex: 1
  },
  scroll: {
    paddingHorizontal: 19,
    paddingBottom: 19
  },
  name: {
    color: colors.black,
    ...sizes.primary,
    fontFamily: family.medium,
    paddingBottom: 7
  },
  about: {
    color: colors.tundora,
    ...sizes.subTitle,
    fontFamily: family.regular
  },
  image: {
    width: 96,
    height: 96,
    borderRadius: 48
  },
  row: { flexDirection: "row" },
  description: { paddingVertical: 19 },
  imageCont: { paddingRight: 24 },
  title: {
    color: colors.black,
    ...sizes.subTitle,
    fontFamily: family.medium,
    paddingBottom: 10
  },
  content: {
    flex: 1
  },
  pubWrap: {
    paddingBottom: 8,
    paddingTop: 8,
    marginVertical: 8
  },
  footer: {
    alignItems: "center",
    paddingBottom: 40,
    paddingHorizontal: 19
  }
});

export default UserProfile;
