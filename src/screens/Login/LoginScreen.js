import React, { createRef, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { View, StyleSheet, Text } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import _ from "lodash";
import { showMessage } from "react-native-flash-message";
import Input from "../../components/Input";
import { family, colors, sizes } from "../../utils/view";
import Button from "../../components/Button";
import useHandleChange from "../../hooks/handleChange";
import { signIn } from "../../actions/auth";
import { removeErrorMessages } from "../../actions/error";
import Loading from "../../components/Loading";

function Login({ navigation }) {
  const scroll = createRef();
  const passwordRef = createRef();

  const email = useHandleChange("");
  const password = useHandleChange("");
  const dispatch = useDispatch();

  const { isLoggedIn, isLoading, error } = useSelector(({ auth }) => ({
    isLoggedIn: auth.isLoggedIn,
    isLoading: auth.isLoading,
    error: auth.error
  }));

  useEffect(() => {
    if (isLoggedIn) {
      navigation.navigate("Favorite");
    }
  }, [isLoggedIn, navigation]);

  useEffect(() => {
    if (!_.isEmpty(error)) {
      showMessage({
        message: error.message,
        type: "danger"
      });
      dispatch(removeErrorMessages());
    }
  }, [error, dispatch]);

  const validateField = useCallback(({ filed, type, length, errorMessage }) => {
    return filed.validate({
      targetValue: filed.value,
      type,
      length,
      errorMessage
    });
  }, []);

  const handleSubmit = useCallback(() => {
    const isValid =
      validateField({ filed: email, type: "email" }) &&
      validateField({ filed: password, type: "password", length: 8 });

    if (isValid) {
      dispatch(signIn({ email: email.value, password: password.value }));
    }
  }, [email, password, dispatch, validateField]);

  const scrollToInput = useCallback(
    reactNode => {
      scroll.current.scrollToFocusedInput(reactNode);
    },
    [scroll]
  );

  if (isLoading) {
    return <Loading />;
  }

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={styles.scrollCont}
      keyboardDismissMode="none"
      keyboardShouldPersistTaps="handled"
      showsVerticalScrollIndicator={false}
      ref={scroll}
    >
      <View style={styles.top}>
        <Text style={styles.title}>Вход</Text>
      </View>
      <View>
        <Input
          scrollToInput={scrollToInput}
          label="E-mail"
          blurOnSubmit={false}
          returnKeyType="next"
          autoCapitalize="none"
          keyboardType="email-address"
          onSubmitEditing={() => passwordRef.current.focus()}
          value={email.value}
          onChangeText={text =>
            email.onChange({
              targetValue: text,
              type: "email"
            })
          }
          error={email.errorMessage}
        />
        <Input
          scrollToInput={scrollToInput}
          label="Пароль"
          blurOnSubmit={false}
          secureTextEntry={true}
          returnKeyType="next"
          autoCapitalize="none"
          onSubmitEditing={handleSubmit}
          inputRef={passwordRef}
          value={password.value}
          onChangeText={text => {
            password.onChange({
              targetValue: text,
              type: "password"
            });
          }}
          error={password.errorMessage}
        />
      </View>
      <View>
        <Text
          style={styles.forgot}
          onPress={() => navigation.navigate("Forgot")}
        >
          Забыли пароль?
        </Text>
      </View>
      <View style={styles.footer}>
        <Button
          onPress={handleSubmit}
          title="Войти"
          container={{ backgroundColor: colors.tapestry, width: 162 }}
          style={{ color: colors.white }}
        />
      </View>
    </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  scrollCont: {
    backgroundColor: colors.white,
    paddingHorizontal: 19,
    paddingBottom: 19,
    flexGrow: 1
  },
  top: {
    paddingBottom: 24
  },
  title: {
    fontFamily: family.medium,
    ...sizes.title,
    color: colors.wineBerry,
    textAlign: "center"
  },
  forgot: {
    ...sizes.warning,
    color: colors.mountainMist,
    fontFamily: family.regular,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  footer: {
    alignItems: "center",
    paddingTop: 40
  }
});

export default Login;
