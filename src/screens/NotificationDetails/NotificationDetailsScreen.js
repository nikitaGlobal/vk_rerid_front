import React, {
  useEffect,
  useCallback,
  useState,
  useRef,
  useMemo
} from "react";
import {
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  Keyboard,
  View,
  ActivityIndicator
} from "react-native";
import { useNavigationParam } from "react-navigation-hooks";
import { useDispatch, useSelector } from "react-redux";
import { DateTime } from "luxon";
import _ from "lodash";
import { colors } from "../../utils/view";
import CommentItem from "../../components/CommentItem";
import api from "../../../api";
import avatar from "../../../assets/img/avatar-placeholder.png";
import CommentInput from "../../components/CommentInput";
import {
  getNotification,
  answerQuestion,
  updatePublitionState
} from "../../actions/notification";
import Loading from "../../components/Loading";
import editionRoles, {
  PENDING_STATE,
  REJECTED_STATE,
  CONFIRMED_STATE
} from "../../utils/editionRoles";
import Button from "../../components/Button";
import { showMessage } from "react-native-flash-message";

function NotificationDetails() {
  const [comment, setComment] = useState("");
  const [isLoading, setLoading] = useState(false);
  const id = useNavigationParam("id");
  const dispatch = useDispatch();
  const { notification, userId } = useSelector(({ notification, user }) => ({
    notification: notification.notification,
    userId: user.user.user.id
  }));
  const user = useSelector(({ user }) => user.user);
  const input = useRef(false);

  useEffect(() => {
    dispatch(getNotification(id));
  }, [dispatch, id]);

  const handleCommentChange = useCallback(text => {
    setComment(text);
  }, []);
  const handleCommentSubmit = useCallback(() => {
    const data = {
      comment,
      to: notification.from,
      comId: notification.id
    };
    setComment("");
    Keyboard.dismiss();
    dispatch(answerQuestion(data));
  }, [comment, dispatch, notification.from, notification.id]);

  const handleFocus = useCallback(() => {
    input.current && input.current.focus();
  }, []);

  const answerImage = useCallback(
    from => {
      if (!notification.send || !notification.invited_user) return avatar;
      if (from === user.user.id) {
        if (user.user.avatar) {
          return { uri: `${api.url}upload/${user.user.avatar}` };
        }
        return avatar;
      }
      if (notification.invited_user.avatar) {
        return { uri: `${api.url}upload/${notification.invited_user.avatar}` };
      }
      return avatar;
    },
    [
      notification.invited_user,
      notification.send,
      user.user.avatar,
      user.user.id
    ]
  );

  const handleConfirm = useCallback(() => {
    setLoading(true);
    dispatch(updatePublitionState(notification.info?.id, CONFIRMED_STATE))
      .then(() => {
        showMessage({
          message: "Статус успешно обновлен",
          type: "success"
        });
      })
      .finally(() => {
        setLoading(false);
      });
  }, [dispatch, notification.info]);

  const handleReject = useCallback(() => {
    setLoading(true);
    dispatch(updatePublitionState(notification.info?.id, REJECTED_STATE))
      .then(() => {
        showMessage({
          message: "Статус успешно обновлен",
          type: "success"
        });
      })
      .finally(() => {
        setLoading(false);
      });
  }, [dispatch, notification.info]);

  const title = useMemo(() => {
    if (!notification.send || !notification.info) return "";
    if (notification.send?.id === userId) {
      return `Вы пригласили ${notification.invited_user?.name} в издательство ${
        notification.info.publication.name
      } на вакансию ${editionRoles[notification.info.role]}`;
    }

    return `Приглашает Вас в издательство ${
      notification.info.publication.name
    } на вакансию ${editionRoles[notification.info.role]}`;
  }, [notification.info, notification.invited_user, notification.send, userId]);

  if (notification.isLoading || _.isEmpty(notification)) {
    return <Loading />;
  }

  return (
    <>
      <ScrollView
        style={styles.scroll}
        contentContainerStyle={styles.container}
        showsVerticalScrollIndicator={false}
      >
        <CommentItem
          name={notification.send?.name}
          title={title}
          text={notification.comment}
          date={DateTime.fromISO(notification.createdAt).toLocaleString(
            DateTime.DATE_MED
          )}
          image={
            notification.send.avatar
              ? { uri: `${api.url}upload/${notification.send.avatar}` }
              : avatar
          }
          onAnswer={handleFocus}
          canAnswer
        >
          {notification.answers.map(item => (
            <CommentItem
              key={item.id}
              name={
                item.from === user.user.id
                  ? user.user.name
                  : notification.invited_user.name
              }
              text={item.comment}
              date={DateTime.fromISO(item.createdAt).toLocaleString(
                DateTime.DATE_MED
              )}
              image={answerImage(item.from)}
            />
          ))}
        </CommentItem>
      </ScrollView>
      {notification.send?.id === userId ? null : isLoading ? (
        <View style={styles.loading}>
          <ActivityIndicator size="large" color={colors.tapestry} />
        </View>
      ) : notification?.info.state === PENDING_STATE ? (
        <View style={styles.footer}>
          <Button
            title="Отконить"
            container={[styles.button, styles.reject]}
            style={styles.buttonText}
            onPress={handleReject}
          />
          <Button
            title="Принять"
            container={[styles.button, styles.confirm]}
            style={styles.buttonText}
            onPress={handleConfirm}
          />
        </View>
      ) : null}
      {notification?.info?.state !== REJECTED_STATE && (
        <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={105}>
          <CommentInput
            onChange={handleCommentChange}
            handleSubmit={handleCommentSubmit}
            value={comment}
            ref={input}
          />
        </KeyboardAvoidingView>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 15,
    paddingBottom: 10
  },
  footer: {
    flexDirection: "row",
    padding: 8
  },
  button: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 8
  },
  reject: {
    backgroundColor: colors.cancel
  },
  confirm: {
    backgroundColor: colors.tapestry
  },
  buttonText: {
    color: colors.white
  },
  loading: {
    height: 60,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default NotificationDetails;
