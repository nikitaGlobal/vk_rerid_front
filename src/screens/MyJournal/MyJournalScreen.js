import React, { useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { View, StyleSheet, SectionList } from "react-native";
import JournalItem, {
  SectionHeader,
  JournalStart
} from "../../components/Journal";
import _ from "lodash";
import { getAllEditions } from "../../actions/edition";
import AddButton from "../../components/AddButton";
import Loading from "../../components/Loading";
import { colors } from "../../utils/view";
import { useNavigation } from "react-navigation-hooks";

function MyJournal() {
  const { navigate } = useNavigation();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllEditions());
  }, [dispatch]);

  const { isLoading, editions } = useSelector(({ edition }) => ({
    isLoading: edition.isLoading,
    editions: edition.editions
  }));

  const renderItem = useCallback(
    ({ item }) => (
      <JournalItem
        image={item.publication.avatar}
        name={item.publication.name}
        color={item.publication.color}
        role={item.role}
        id={item.pubId}
      />
    ),
    []
  );

  const renderSectionHeader = useCallback(
    ({ section: { title } }) => <SectionHeader title={title} />,
    []
  );

  const handleAddPress = useCallback(() => {
    navigate("JournalDetails");
  }, [navigate]);

  const listEmptyComponent = useCallback(
    () => <JournalStart handleAddPress={handleAddPress} />,
    [handleAddPress]
  );

  if (isLoading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <SectionList
        renderItem={renderItem}
        renderSectionHeader={renderSectionHeader}
        contentContainerStyle={styles.scroll}
        sections={editions}
        keyExtractor={(item, index) => item + index}
        ListEmptyComponent={listEmptyComponent}
        showsVerticalScrollIndicator={false}
        stickySectionHeadersEnabled
      />
      {!_.isEmpty(editions) && (
        <AddButton title="Добавить издание" onPress={handleAddPress} />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  scroll: {
    padding: 15
  }
});

export default MyJournal;
