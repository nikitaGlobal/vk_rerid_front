import React, { useEffect, useCallback, memo, useState } from "react";
import { DateTime } from "luxon";
import { useDispatch, useSelector } from "react-redux";
import { StyleSheet, Text, FlatList, View, RefreshControl } from "react-native";
import { colors, sizes, family } from "../../utils/view";
import { getPopularByCatId } from "../../actions/popular";
import ListItemWithMenu from "../../components/ListItemWithMenu";
import { useNavigationParam, useNavigation } from "react-navigation-hooks";
import Loading from "../../components/Loading";

function ListItemComponent({
  id,
  name,
  avatar,
  createdAt,
  description,
  navigate
}) {
  const handlePress = useCallback(() => {
    navigate("PopularItem", { title: name, id });
  }, [id, name, navigate]);

  return (
    <>
      <ListItemWithMenu
        key={id}
        onPress={handlePress}
        name={name}
        image={avatar}
        role={DateTime.fromISO(createdAt).toLocaleString(DateTime.DATE_MED)}
        border
      />
      <Text style={styles.description}>{description}</Text>
    </>
  );
}

ListItemComponent.defaultProps = {
  description: ""
};

export const ListItem = memo(ListItemComponent);

function Popular() {
  const id = useNavigationParam("id");
  const { navigate } = useNavigation();
  const dispatch = useDispatch();
  const populars = useSelector(({ popular }) => popular.populars);
  const [isLoading, setLoading] = useState(false);
  const [isRefreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await dispatch(getPopularByCatId(id));
    setRefreshing(false);
  }, [dispatch, id]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await dispatch(getPopularByCatId(id));
      setLoading(false);
    })();
  }, [dispatch, id]);

  const renderItem = useCallback(
    ({ item }) => (
      <ListItem
        id={item.id}
        navigate={navigate}
        name={item.name}
        avatar={item.avatar}
        createdAt={item.createdAt}
        description={item.description}
      />
    ),
    [navigate]
  );

  const renderEmptyComponent = useCallback(
    () => (
      <View style={styles.empty}>
        <Text style={styles.text}>Пока нет изданиий</Text>
      </View>
    ),
    []
  );

  if (isLoading) {
    return <Loading />;
  }
  return (
    <FlatList
      data={populars.sort((a, b) => {
        a = new Date(a.createdAt);
        b = new Date(b.createdAt);
        return a > b ? -1 : a < b ? 1 : 0;
      })}
      contentContainerStyle={styles.container}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      ListEmptyComponent={renderEmptyComponent}
      refreshControl={
        <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
      }
    />
  );
}

Popular.defaultProps = {
  isLoading: false,
  error: {}
};

const styles = StyleSheet.create({
  empty: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: "center",
    justifyContent: "center",
    padding: 15
  },
  container: {
    flexGrow: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 20
  },
  text: {
    textAlign: "center",
    fontFamily: family.regular,
    fontSize: 18
  },
  description: {
    ...sizes.list,
    color: colors.black,
    paddingBottom: 10,
    fontFamily: family.regular
  }
});

export default Popular;
