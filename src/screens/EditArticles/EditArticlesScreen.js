/* eslint-disable react-native/no-color-literals */
import React, {
  useRef,
  useCallback,
  useEffect,
  useState,
  useMemo
} from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Easing,
  TouchableWithoutFeedback,
  Animated,
  Text,
  Image
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import ImagePicker from "react-native-image-crop-picker";
import _ from "lodash";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Toast from "react-native-easy-toast";
import { AudioRecorder, AudioUtils } from "react-native-audio";
import { showMessage } from "react-native-flash-message";
import Input from "../../components/Input";
import { colors } from "../../utils/view";
import Button from "../../components/Button";
import useHandleChange from "../../hooks/handleChange";
import { uploadAttachments } from "../../actions/file";
import Loading from "../../components/Loading";
import {
  updateArticle,
  getArticleById,
  updateTyoeArticle
} from "../../actions/article";
import { removeErrorMessages } from "../../actions/error";
import AudioTrack from "../../components/AudioTrack";
import { useNavigationParam } from "react-navigation-hooks";
import { mmssmm } from "../../utils/mmss";
import api from "../../../api";
import { ARTICLE_DRAFT } from "../../utils/editionRoles";

const microphoneAnim = new Animated.Value(0);

function EditArticle() {
  const dispatch = useDispatch();
  const { isLoading, error, article } = useSelector(({ article, user }) => ({
    isLoading: user.isLoading,
    error: article.error,
    article: article.article
  }));
  const id = useNavigationParam("id");
  const scroll = useRef(null);
  const descriptionRef = useRef(null);
  const toast = useRef(null);
  const [images, setImages] = useState(article.articleImages || []);
  const name = useHandleChange(article.name);
  const description = useHandleChange(article.description);
  const [audios, setAudios] = useState(article.articleAudios || []);
  const [recordTime, setRecordTime] = useState("00:00:00");

  useEffect(() => {
    dispatch(getArticleById(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (!_.isEmpty(error)) {
      showMessage({
        message: error.message,
        type: "danger"
      });
      dispatch(removeErrorMessages());
    }
  }, [dispatch, error]);

  const finishAudio = useCallback(
    data => {
      audios.push(data);
      setAudios([...audios]);
    },
    [audios]
  );

  useEffect(() => {
    AudioRecorder.requestAuthorization().then(isAuthorized => {
      // this.setState({ hasPermission: isAuthorized });

      if (!isAuthorized) return;

      AudioRecorder.onProgress = data => {
        setRecordTime(mmssmm(data.currentTime * 1000));
      };

      AudioRecorder.onFinished = data => {
        let requestData = {};
        // Android callback comes in the form of a promise instead.
        const mimeArr = data.audioFileURL.split(".");
        const filenameArr = data.audioFileURL.split("/");
        const filename = filenameArr[filenameArr.length - 1];
        const mime = `audio/${mimeArr[mimeArr.length - 1]}`;
        requestData = {
          path: data.audioFileURL,
          filename,
          mime
        };
        upLoadFile(requestData).then(res => {
          finishAudio(res[0]);
        });
      };
    });
  }, [dispatch, finishAudio, upLoadFile]);

  const handleSubmit = useCallback(() => {
    const isValid = name.validate({ filed: name, type: "length", length: 2 });

    if (isValid) {
      const data = {
        name: name.value,
        id,
        pubId: article.pubId,
        files: { audios, images }
      };
      if (description.value) {
        data.description = description.value;
      }
      dispatch(updateArticle(data));
    }
  }, [article, name, id, audios, images, description.value, dispatch]);

  const scrollToInput = useCallback(reactNode => {
    scroll.current.scrollToFocusedInput(reactNode);
  }, []);

  const saveDraft = useCallback(() => {
    dispatch(updateTyoeArticle(id, article.pubId, ARTICLE_DRAFT));
  }, [article.pubId, dispatch, id]);

  const removeImages = useCallback(
    id => {
      const index = images.findIndex(x => x.id === id);
      if (index > -1) {
        images.splice(index, 1);
      }
      if (_.isEmpty(images)) {
        setImages([]);
        return;
      }
      setImages(...images);
    },
    [images]
  );

  const handlePressIn = useCallback(() => {
    microphoneAnim.setValue(0);
    Animated.timing(microphoneAnim, {
      toValue: 1,
      duration: 150,
      easing: Easing.back(2)
    }).start();
    onStartRecord();
  }, [onStartRecord]);

  const handlePressOut = useCallback(() => {
    Animated.timing(microphoneAnim, {
      toValue: 0,
      duration: 150,
      easing: Easing.back(2)
    }).start();
    onStopRecord();
    setRecordTime("00:00:00");
  }, [onStopRecord]);

  const scale = useMemo(
    () =>
      microphoneAnim.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 1.5]
      }),
    []
  );

  const onStartRecord = useCallback(async () => {
    let audioPath =
      AudioUtils.DocumentDirectoryPath + `/file-${Date.now()}.aac`;
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac"
    });
    await AudioRecorder.startRecording();
  }, []);

  const onStopRecord = useCallback(async () => {
    await AudioRecorder.stopRecording();
  }, []);

  const upLoadFile = useCallback(
    file => {
      return dispatch(uploadAttachments(file));
    },
    [dispatch]
  );
  const handleImagePress = useCallback(() => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      compressImageQuality: 0.8,
      cropperToolbarTitle: "Move and Scale",
      useFrontCamera: true
    }).then(imageValue => {
      upLoadFile(imageValue).then(res => {
        images.push(res[0]);
        setImages([...images]);
      });
    });
  }, [images, upLoadFile]);
  if (isLoading) {
    return <Loading />;
  }
  return (
    <View style={styles.root}>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollCont}
        keyboardDismissMode="none"
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        ref={scroll}
        style={styles.scroll}
      >
        <View style={styles.container}>
          <View>
            <Input
              scrollToInput={scrollToInput}
              label="Заголовок статьи"
              blurOnSubmit={false}
              returnKeyType="next"
              onSubmitEditing={() => descriptionRef.current.focus()}
              value={name.value}
              onChangeText={text =>
                name.onChange({
                  targetValue: text,
                  type: "length",
                  length: 2,
                  errMessage: "Имя должно содержать не менее 2 символов"
                })
              }
              error={name.errorMessage}
            />
            <Input
              scrollToInput={scrollToInput}
              label="Текст статьи"
              blurOnSubmit={false}
              inputRef={descriptionRef}
              value={description.value}
              inputStyles={styles.multiline}
              onChangeText={text => {
                description.onChange({
                  targetValue: text
                });
              }}
              error={description.errorMessage}
              multiline
            />
          </View>
          {audios.map((item, key) => (
            <View key={key.toString()} style={styles.imgWrap}>
              <AudioTrack audio={`${api.url}upload/audio/${item.filename}`} />
              <TouchableOpacity onPress={() => removeImages(item.id)}>
                <Icon name="trash-can" size={25} color={colors.amethystSmoke} />
              </TouchableOpacity>
            </View>
          ))}
          {images.map(item => (
            <View key={item.filename} style={styles.imgWrap}>
              <Image
                source={{ uri: `${api.url}upload/${item.filename}` }}
                style={styles.image}
              />
              <TouchableOpacity onPress={() => removeImages(item.id)}>
                <Icon name="trash-can" size={25} color={colors.amethystSmoke} />
              </TouchableOpacity>
            </View>
          ))}
          <View style={styles.actions}>
            {recordTime !== "00:00:00" && (
              <View style={styles.timeRecord}>
                <View style={styles.recordIcon} />
                <Text style={styles.recordText}>{recordTime}</Text>
              </View>
            )}
            <TouchableWithoutFeedback
              onPressIn={handlePressIn}
              onPressOut={handlePressOut}
            >
              <Animated.View style={[styles.icon, { transform: [{ scale }] }]}>
                <Icon name="microphone" size={25} color={colors.white} />
              </Animated.View>
            </TouchableWithoutFeedback>
            <TouchableOpacity onPress={handleImagePress} style={styles.icon}>
              <Icon name="camera" size={25} color={colors.white} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.footer}>
          <Button
            onPress={handleSubmit}
            title="Отправить"
            container={{
              backgroundColor: colors.tapestry,
              paddingHorizontal: 40
            }}
            style={{ color: colors.white }}
          />
          <Button
            onPress={saveDraft}
            title="Сохранить в черновиках"
            container={{
              backgroundColor: colors.prim,
              marginTop: 24,
              paddingHorizontal: 30
            }}
            style={{ color: colors.wineBerry }}
          />
        </View>
      </KeyboardAwareScrollView>
      <Toast ref={toast} position="bottom" positionValue={250} opacity={0.8} />
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: colors.white
  },
  container: {
    flex: 1
  },
  scroll: {
    paddingHorizontal: 19
  },
  scrollCont: {
    flexGrow: 1
  },
  footer: {
    alignItems: "center",
    paddingBottom: 20
  },
  multiline: { height: 120 },
  actions: {
    flexDirection: "row",
    paddingVertical: 30,
    position: "relative"
  },
  icon: {
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20,
    backgroundColor: colors.tapestry,
    marginRight: 30
  },
  timeRecord: {
    flexDirection: "row",
    alignItems: "center",
    position: "absolute",
    left: 0,
    top: -3
  },
  recordIcon: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: "red",
    marginRight: 5
  },
  image: {
    height: 231,
    resizeMode: "cover",
    flex: 1
  },
  imgWrap: {
    flexDirection: "row",
    alignItems: "flex-end",
    paddingBottom: 10
  }
});

export default EditArticle;
