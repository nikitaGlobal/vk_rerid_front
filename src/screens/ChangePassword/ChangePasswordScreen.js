import React, { useRef, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { View, StyleSheet } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Input from "../../components/Input";
import { colors } from "../../utils/view";
import Button from "../../components/Button";
import Loading from "../../components/Loading";
import useHandleChange from "../../hooks/handleChange";
import { editPassword } from "../../actions/user";

function ChangePassword({ navigation }) {
  const scroll = useRef(null);
  const passwordRef = useRef(null);
  const confirmPasswordRef = useRef(null);
  const dispatch = useDispatch();

  const { isLoading } = useSelector(({ user }) => ({
    isLoading: user.isLoading
  }));

  const currentPass = useHandleChange("");
  const password = useHandleChange("");
  const confirmPassword = useHandleChange("");

  const validateField = useCallback(
    ({ filed, type, length, secondValue, errorMessage }) => {
      return filed.validate({
        targetValue: filed.value,
        type,
        length,
        secondValue,
        errorMessage
      });
    },
    []
  );

  const handleSubmit = useCallback(() => {
    const isValid =
      validateField({ filed: currentPass, type: "password", length: 8 }) &&
      validateField({ filed: password, type: "password", length: 8 }) &&
      validateField({
        filed: confirmPassword,
        type: "equal",
        secondValue: password.value,
        errMessage: "Passwords do not match"
      });
    if (isValid) {
      dispatch(
        editPassword(
          {
            currentPass: currentPass.value,
            password: password.value
          },
          navigation
        )
      );
    }
  }, [
    confirmPassword,
    currentPass,
    dispatch,
    navigation,
    password,
    validateField
  ]);

  const scrollToInput = useCallback(reactNode => {
    scroll.current.scrollToFocusedInput(reactNode);
  }, []);

  if (isLoading) {
    return <Loading />;
  }
  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.content}
        keyboardDismissMode="none"
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        ref={scroll}
      >
        <View style={styles.content}>
          <Input
            scrollToInput={scrollToInput}
            label="Введите старый пароль"
            blurOnSubmit={false}
            secureTextEntry={true}
            returnKeyType="next"
            autoCapitalize="none"
            onSubmitEditing={() => passwordRef.current.focus()}
            inputRef={passwordRef}
            value={currentPass.value}
            onChangeText={text => {
              currentPass.onChange({
                targetValue: text,
                type: "password"
              });
            }}
            error={currentPass.errorMessage}
          />
          <Input
            scrollToInput={scrollToInput}
            label="Введите новый пароль"
            blurOnSubmit={false}
            secureTextEntry={true}
            returnKeyType="next"
            autoCapitalize="none"
            onSubmitEditing={() => confirmPasswordRef.current.focus()}
            inputRef={passwordRef}
            value={password.value}
            onChangeText={text => {
              password.onChange({
                targetValue: text,
                type: "password"
              });
            }}
            error={password.errorMessage}
          />
          <Input
            scrollToInput={scrollToInput}
            label="Введите новый пароль повторно"
            blurOnSubmit={true}
            secureTextEntry={true}
            returnKeyType="done"
            autoCapitalize="none"
            onSubmitEditing={handleSubmit}
            inputRef={confirmPasswordRef}
            value={confirmPassword.value}
            onChangeText={text =>
              confirmPassword.onChange({
                targetValue: text,
                type: "equal",
                secondValue: password.value,
                errMessage: "Пароли не совпадают"
              })
            }
            error={confirmPassword.errorMessage}
          />
        </View>
        <View style={styles.button}>
          <Button
            onPress={handleSubmit}
            title="Сохранить изменения"
            container={{ backgroundColor: colors.tapestry }}
            style={{ color: colors.white }}
          />
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 19,
    paddingBottom: 19
  },
  content: {
    flex: 1
  },
  button: {
    paddingTop: 20
  }
});

export default ChangePassword;
