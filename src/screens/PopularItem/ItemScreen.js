import React, { useEffect, useCallback, memo, useState, useMemo } from "react";
import {
  StyleSheet,
  Text,
  FlatList,
  View,
  Dimensions,
  RefreshControl
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { DateTime } from "luxon";
import _ from "lodash";
import ListItemWithMenu from "../../components/ListItemWithMenu";
import { ItemView } from "../../components/Publications";
import { getPopularById } from "../../actions/popular";
import AudioTrack from "../../components/AudioTrack";
import Loading from "../../components/Loading";

import { colors, sizes, family } from "../../utils/view";
import { Img } from "../../components/Custom";
import api from "../../../api";

import message from "../../../assets/img/icons/messege.png";
import dislike from "../../../assets/img/icons/dislike.png";
import dislike_active from "../../../assets/img/icons/dislike_active.png";
import like from "../../../assets/img/icons/like.png";
import like_active from "../../../assets/img/icons/like_active.png";
import Feedback from "../../components/Feedback";
import { useNavigation, useNavigationParam } from "react-navigation-hooks";
import {
  connectToSocket,
  disconnectFromSocket,
  socket
} from "../../utils/socket";
import { SET_FEEDBACK } from "../../constants/popular";

function ListItemComponent({
  id,
  pubId,
  name,
  avatar,
  createdAt,
  description,
  images,
  audios,
  feedbacks = [],
  likesCount = 0,
  dislikesCount = 0,
  commentCount = 0
}) {
  const dispatch = useDispatch();
  const userId = useSelector(({ user }) => user.user.user.id);
  const { navigate } = useNavigation();

  const action = useMemo(() => {
    const index = feedbacks.findIndex(item => item.userId === userId);

    if (index > -1) {
      if (feedbacks[index].like) return "like";
      return "dislike";
    }

    return "";
  }, [feedbacks, userId]);

  const handlePressAction = useCallback(
    value => {
      const isLike = value === "like";
      const data = {
        articleId: id,
        userId,
        like: isLike ? 1 : 0,
        dislike: isLike ? 0 : 1,
        isMyAction: true
      };

      socket.emit("feedback", data);
      dispatch({
        type: SET_FEEDBACK,
        payload: { ...data, currentAction: action }
      });
    },
    [action, dispatch, id, userId]
  );

  return (
    <>
      <ListItemWithMenu
        key={id}
        name={name}
        image={avatar}
        role={DateTime.fromISO(createdAt).toLocaleString(DateTime.DATE_MED)}
        onPress={() => navigate("ArticleDetails", { id, pubId })}
        border
      />
      <View style={styles.wrapInfos}>
        {!_.isEmpty(images) &&
          images.map(item => {
            return (
              <View key={item.filename} style={styles.imgWrap}>
                <Img image={item.filename} url style={styles.image} />
              </View>
            );
          })}
        {!_.isEmpty(audios) &&
          audios.map(item => {
            return (
              <View key={item.filename} style={styles.imgWrap}>
                <AudioTrack audio={`${api.url}upload/audio/${item.filename}`} />
              </View>
            );
          })}
      </View>
      <Text style={styles.description}>{description}</Text>
      <View style={styles.wrap}>
        <Feedback
          image={message}
          count={commentCount}
          onPress={() => navigate("ArticleDetails", { id, pubId })}
        />
        <Feedback
          image={action === "like" ? like_active : like}
          disabled={action === "like"}
          count={likesCount}
          onPress={() => handlePressAction("like")}
        />
        <Feedback
          image={action === "dislike" ? dislike_active : dislike}
          disabled={action === "dislike"}
          count={dislikesCount}
          onPress={() => handlePressAction("dislike")}
        />
      </View>
    </>
  );
}

ListItemComponent.defaultProps = {
  description: "",
  images: [],
  audios: []
};

export const ListItem = memo(ListItemComponent);

function PopularItem() {
  const id = useNavigationParam("id");
  const dispatch = useDispatch();
  const popular = useSelector(({ popular }) => popular.popular);
  const [isLoading, setLoading] = useState(false);
  const [isRefreshing, setRefreshing] = useState(false);

  const handleGetFeedback = useCallback(
    data => {
      dispatch({ type: SET_FEEDBACK, payload: { ...data, isMyAction: false } });
    },
    [dispatch]
  );

  useEffect(() => {
    if (popular && popular.name) {
      connectToSocket(id, popular.name, () => {
        socket.on("feedback", handleGetFeedback);
      });
      return () => {
        disconnectFromSocket();
      };
    }
  }, [handleGetFeedback, id, popular]);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await dispatch(getPopularById(id));
    setRefreshing(false);
  }, [dispatch, id]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await dispatch(getPopularById(id));
      setLoading(false);
    })();
  }, [dispatch, id]);

  const renderItem = useCallback(
    ({ item }) => {
      if (!item) return null;
      return (
        <ListItem
          id={item.id}
          name={item.name}
          avatar={popular?.avatar}
          audios={item.articleAudios}
          images={item.articleImages}
          pubId={id}
          createdAt={item.createdAt}
          description={item.description}
          feedbacks={item.feedbacks}
          likesCount={item.like}
          dislikesCount={item.dislike}
          commentCount={item.commentsCount}
        />
      );
    },
    [id, popular]
  );

  const renderEmptyComponent = useCallback(
    () => (
      <View style={styles.empty}>
        <Text style={styles.text}>Пока нет статей</Text>
      </View>
    ),
    []
  );

  if (isLoading || !popular) {
    return <Loading />;
  }
  return (
    <View style={styles.container}>
      <ItemView
        name={popular.name}
        id={popular.id}
        favoriteItem={popular.favoriteItem?.pubId}
        description={popular.description}
        image={popular.avatar}
        style={styles.head}
      />
      <FlatList
        data={popular.articles}
        contentContainerStyle={styles.flatList}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        ListEmptyComponent={renderEmptyComponent}
        refreshControl={
          <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
        }
      />
    </View>
  );
}

PopularItem.defaultProps = {
  isLoading: false,
  error: {}
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  empty: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: "center",
    justifyContent: "center",
    padding: 15
  },
  text: {
    textAlign: "center",
    fontFamily: family.regular,
    fontSize: 18
  },
  flatList: {
    flexGrow: 1,
    paddingHorizontal: 20
  },
  wrap: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    paddingVertical: 3,
    borderColor: colors.mercury,
    borderBottomWidth: 1,
    borderTopWidth: 1
  },
  description: {
    ...sizes.list,
    color: colors.black,
    paddingVertical: 10,
    fontFamily: family.regular
  },
  image: {
    height: 231,
    width: Dimensions.get("window").width - 40,
    resizeMode: "cover"
  },
  wrapInfos: {
    flex: 1
  },
  imgWrap: {
    flexDirection: "row",
    alignItems: "flex-end",
    paddingBottom: 10
  },
  head: {
    paddingHorizontal: 20
  }
});

export default PopularItem;
