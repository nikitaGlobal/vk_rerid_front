import React, { useRef, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { View, StyleSheet, Text, Alert } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import _ from "lodash";
import { showMessage } from "react-native-flash-message";
import Input from "../../components/Input";
import { family, colors, sizes } from "../../utils/view";
import Button from "../../components/Button";
import Loading from "../../components/Loading";
import useHandleChange from "../../hooks/handleChange";
import { signUp, clearData } from "../../actions/auth";
import { removeErrorMessages } from "../../actions/error";

function Register({ navigation }) {
  const dispatch = useDispatch();
  const { isSignup, isLoading, error } = useSelector(({ auth }) => ({
    isSignup: auth.signup,
    isLoading: auth.isLoading,
    error: auth.error
  }));
  const scroll = useRef(null);
  const passwordRef = useRef(null);
  const confirmPasswordRef = useRef(null);

  const email = useHandleChange("");
  const password = useHandleChange("");
  const confirmPassword = useHandleChange("");

  useEffect(() => {
    if (isSignup) {
      Alert.alert(
        "Поздравляем",
        "Вы успешно зарегистрированы, пожалуйста, проверьте вашу электронную почту для дальнейших инструкций",
        [
          {
            text: "ОК",
            onPress: () => {
              dispatch(clearData());
              navigation.navigate("Login");
            }
          }
        ],
        { cancelable: false }
      );
    }
  }, [dispatch, isSignup, navigation]);

  useEffect(() => {
    if (!_.isEmpty(error)) {
      showMessage({
        message: error.message,
        type: "danger"
      });
      dispatch(removeErrorMessages());
    }
  }, [error, dispatch]);

  const validateField = useCallback(
    ({ filed, type, length, secondValue, errorMessage }) =>
      filed.validate({
        targetValue: filed.value,
        type,
        length,
        secondValue,
        errorMessage
      }),
    []
  );

  const handleSubmit = useCallback(() => {
    const isValid =
      validateField({ filed: email, type: "email" }) &&
      validateField({ filed: password, type: "password", length: 8 }) &&
      validateField({
        filed: confirmPassword,
        type: "equal",
        secondValue: password.value,
        errMessage: "Passwords do not match"
      });
    if (isValid) {
      dispatch(signUp({ email: email.value, password: password.value }));
    }
  }, [confirmPassword, dispatch, email, password, validateField]);

  const scrollToInput = useCallback(reactNode => {
    scroll.current.scrollToFocusedInput(reactNode);
  }, []);
  if (isLoading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollCont}
        keyboardDismissMode="none"
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        ref={scroll}
      >
        <View style={styles.top}>
          <Text style={styles.title}>Создайте учетную запись</Text>
        </View>
        <View>
          <Input
            scrollToInput={scrollToInput}
            label="E-mail"
            blurOnSubmit={false}
            returnKeyType="next"
            autoCapitalize="none"
            keyboardType="email-address"
            onSubmitEditing={() => passwordRef.current.focus()}
            value={email.value}
            onChangeText={text =>
              email.onChange({
                targetValue: text,
                type: "email"
              })
            }
            error={email.errorMessage}
          />
          <Input
            scrollToInput={scrollToInput}
            label="Пароль"
            blurOnSubmit={false}
            secureTextEntry={true}
            returnKeyType="next"
            autoCapitalize="none"
            onSubmitEditing={() => confirmPasswordRef.current.focus()}
            inputRef={passwordRef}
            value={password.value}
            onChangeText={text => {
              password.onChange({
                targetValue: text,
                type: "password"
              });
            }}
            error={password.errorMessage}
          />
          <Input
            scrollToInput={scrollToInput}
            label="Повторите пароль"
            blurOnSubmit={true}
            secureTextEntry={true}
            returnKeyType="done"
            autoCapitalize="none"
            onSubmitEditing={handleSubmit}
            inputRef={confirmPasswordRef}
            value={confirmPassword.value}
            onChangeText={text =>
              confirmPassword.onChange({
                targetValue: text,
                type: "equal",
                secondValue: password.value,
                errMessage: "Пароли не совпадают"
              })
            }
            error={confirmPassword.errorMessage}
          />
        </View>
        <View>
          <Text style={styles.warning}>
            Зарегистрировавшись, вы соглашаетесь с{" "}
            <Text style={styles.underline}>политикой конфиденциальности</Text> и{" "}
            <Text style={styles.underline}>условиями предоставления</Text> услуг
          </Text>
        </View>
        <View style={styles.footer}>
          <Button
            onPress={handleSubmit}
            title="Отправить"
            container={{ backgroundColor: colors.tapestry, width: 162 }}
            style={{ color: colors.white }}
          />
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 19,
    paddingBottom: 19
  },
  scrollCont: { flex: 1 },
  top: { paddingBottom: 24 },
  underline: { textDecorationLine: "underline" },
  title: {
    fontFamily: family.medium,
    ...sizes.title,
    color: colors.wineBerry,
    textAlign: "center"
  },
  warning: {
    ...sizes.warning,
    color: colors.mountainMist,
    fontFamily: family.regular
  },
  footer: { alignItems: "center", paddingTop: 40 }
});

export default Register;
