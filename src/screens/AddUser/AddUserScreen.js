import React, {
  useRef,
  useState,
  useCallback,
  useEffect,
  useMemo
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { View, StyleSheet } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import _ from "lodash";
import { showMessage } from "react-native-flash-message";
import Input from "../../components/Input";
import { colors } from "../../utils/view";
import Button from "../../components/Button";
import useHandleChange from "../../hooks/handleChange";
import Select from "../../components/Select";
import { inviteUser } from "../../actions/edition";
import { removeErrorMessages } from "../../actions/error";
import roles, { EDITOR } from "../../utils/editionRoles";

function AddUser({ navigation }) {
  const dispatch = useDispatch();
  const { isLoading, error } = useSelector(({ edition }) => ({
    isLoading: edition.isLoading,
    error: edition.error
  }));
  const scroll = useRef(null);
  const comment = useHandleChange("");
  const [role, setRole] = useState("");

  useEffect(() => {
    if (!_.isEmpty(error)) {
      showMessage({
        message: error.message,
        type: "danger"
      });
      dispatch(removeErrorMessages());
    }
  }, [dispatch, error]);

  const handleSubmit = useCallback(() => {
    const isValid = comment.validate({
      filed: comment,
      type: "length",
      length: 2
    });

    if (isValid) {
      const data = {
        comment: comment.value,
        role: role,
        pubId: navigation.getParam("pubId"),
        userId: navigation.getParam("userId")
      };
      dispatch(inviteUser(data)).then(res => {
        if (res.status === "Error") {
          showMessage({
            message: "Пользователь уже есть",
            type: "success"
          });
        } else {
          showMessage({
            message: "Приглашение отправлено",
            type: "success"
          });
        }
        setTimeout(() => {
          navigation.navigate("Home");
        }, 500);
      });
    }
  }, [comment, dispatch, navigation, role]);

  const scrollToInput = useCallback(reactNode => {
    scroll.current.scrollToFocusedInput(reactNode);
  }, []);

  const handleSelect = useCallback(value => {
    setRole(value);
  }, []);

  const rolesData = useMemo(() => {
    const data = [];
    for (let key in roles) {
      if (!roles.hasOwnProperty(key) || key === EDITOR) continue;

      data.push({ value: key, label: roles[key] });
    }
    return data;
  }, []);

  return (
    <View style={styles.root}>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollCont}
        keyboardDismissMode="none"
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        ref={scroll}
        style={styles.scroll}
      >
        <View style={styles.container}>
          <View>
            <Select
              label="Вакансия"
              options={rolesData}
              value={role}
              onChangeText={handleSelect}
            />
            <Input
              scrollToInput={scrollToInput}
              label="Комментарий"
              blurOnSubmit={false}
              returnKeyType="next"
              onSubmitEditing={handleSubmit}
              value={comment.value}
              onChangeText={text =>
                comment.onChange({
                  targetValue: text,
                  type: "length",
                  length: 2,
                  errMessage: "Имя должно содержать не менее 2 символов"
                })
              }
              error={comment.errorMessage}
            />
          </View>
        </View>
        <View style={styles.footer}>
          <Button
            onPress={handleSubmit}
            title="Отправить"
            container={{ backgroundColor: colors.tapestry, width: "100%" }}
            style={{ color: colors.white }}
            isLoading={isLoading}
          />
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: colors.white
  },
  container: {
    flex: 1
  },
  scroll: {
    paddingHorizontal: 19
  },
  scrollCont: {
    flex: 1
  },
  footer: {
    alignItems: "center",
    paddingBottom: 20
  }
});

export default AddUser;
