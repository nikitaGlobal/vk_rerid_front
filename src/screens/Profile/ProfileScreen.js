import React, { useEffect, useState, useCallback } from "react";
import { View, StyleSheet, Text, ScrollView, AsyncStorage } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { showMessage } from "react-native-flash-message";
import { colors, sizes, family } from "../../utils/view";
import ListItem from "../../components/ListItem";
import Loading from "../../components/Loading";
import Button from "../../components/Button";
import { removeErrorMessages } from "../../actions/error";
import { Img } from "../../components/Custom";
import { logOut } from "../../actions/auth";
import notifications from "../../../assets/img/icons/notifications.png";
// import comment from "../../../assets/img/icons/comment.png";
// import answer from "../../../assets/img/icons/answer.png";
import { useNavigation } from "react-navigation-hooks";

function Profile() {
  const { navigate } = useNavigation();
  const [guest, setGuestValue] = useState("loading");
  const dispatch = useDispatch();
  const { isLoading, error, user, viewedCount } = useSelector(
    ({ user, notification }) => ({
      isLoading: user.isLoading,
      error: user.error,
      user: user.user,
      viewedCount: notification.viewedCount
    })
  );

  useEffect(() => {
    AsyncStorage.getItem("token").then(token => {
      if (token === "guest") {
        setGuestValue("guest");
      } else {
        setGuestValue("user");
      }
    });
  }, []);

  useEffect(() => {
    AsyncStorage.getItem("nottifications").then(res => {
      dispatch({ type: "SET_VIEWED_COUNT", payload: res || 0 });
    });
  }, [dispatch]);

  useEffect(() => {
    if (!_.isEmpty(error)) {
      showMessage({
        message: error.message,
        type: "danger"
      });
      dispatch(removeErrorMessages());
    }
  }, [dispatch, error]);

  const handlePressNotifications = useCallback(() => {
    navigate("Notifications");
  }, [navigate]);

  if (
    (isLoading || _.isEmpty(user) || guest === "loading") &&
    guest !== "guest"
  ) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <ScrollView
        contentContainerStyle={styles.scrollCont}
        style={styles.scroll}
      >
        {guest === "guest" ? (
          <View style={styles.guestContainer}>
            <View style={styles.alCenter}>
              <Img style={styles.image} url />
            </View>
            <View style={styles.top}>
              <Text style={[styles.about, styles.textCenter]}>
                Чтобы пользоваться всеми функциями приложения зарегестрируйтесь
                или войдите в свой аккаунт
              </Text>
            </View>
            <View style={styles.alCenter}>
              <Button
                onPress={() => {
                  dispatch(logOut());
                  navigate("Register");
                }}
                title="Создать учетную запись"
                container={{
                  backgroundColor: colors.tapestry,
                  paddingHorizontal: 21
                }}
                style={{ color: colors.white }}
              />
              <Button
                onPress={() => {
                  dispatch(logOut());

                  navigate("Login");
                }}
                title="Войти"
                container={{
                  backgroundColor: colors.prim,
                  marginTop: 24,
                  paddingHorizontal: 31
                }}
                style={{ color: colors.wineBerry }}
              />
            </View>
          </View>
        ) : (
          <>
            <View style={styles.row}>
              <View style={styles.imageCont}>
                <Img image={user.user.avatar} style={styles.image} url />
              </View>
              <View>
                <Text style={styles.name}>{user.user.name}</Text>
              </View>
            </View>
            <View style={styles.description}>
              <Text style={styles.about}>{user.user.description}</Text>
            </View>
            <View style={styles.listCont}>
              {/* <ListItem icon={comment} text="Комментарии" onPress={() => {}} />
              <ListItem
                icon={answer}
                text="Ответы"
                count={12}
                onPress={() => {}}
              /> */}
              <ListItem
                icon={notifications}
                text="Уведомления"
                disabled={!user.user.comment.length}
                count={
                  user.user.comment.length - viewedCount < 0
                    ? 0
                    : user.user.comment.length - viewedCount
                }
                onPress={handlePressNotifications}
              />
            </View>
          </>
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  scrollCont: {
    flex: 1
  },
  scroll: {
    paddingHorizontal: 19,
    paddingBottom: 19
  },
  top: {
    marginVertical: 36
  },
  name: {
    color: colors.black,
    ...sizes.primary,
    fontFamily: family.medium,
    paddingBottom: 7
  },
  about: {
    color: colors.tundora,
    ...sizes.subTitle,
    fontFamily: family.regular
  },
  guestContainer: {
    justifyContent: "center",
    flex: 1
  },
  alCenter: {
    alignItems: "center"
  },
  image: {
    width: 96,
    height: 96,
    borderRadius: 48
  },
  row: { flexDirection: "row" },
  textCenter: { textAlign: "center" },
  description: { paddingVertical: 19 },
  imageCont: { paddingRight: 24 },
  listCont: { paddingTop: 20 }
});

export default Profile;
