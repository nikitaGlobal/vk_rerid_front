import React from "react";
import { View, StyleSheet, Text, Image, ScrollView } from "react-native";
import { PreHeader, Comment } from "../../components/Journal";
import { family, colors, sizes } from "../../utils/view";
import image from "../../../assets/img/images.jpeg";
import imageTravel from "../../../assets/img/getty_583734066_335273.jpg";

const data = [
  {
    image
  },
  {
    image: imageTravel
  }
];

function Journal() {
  return (
    <>
      <ScrollView style={styles.container}>
        <PreHeader name="Неновости" date="13 марта 7:12" />
        <View style={styles.top}>
          <Text style={styles.author}>Автор Иван Абрамов</Text>
          <Text style={styles.title}>
            Более 1,2 тыс. памятников архитектуры отреставрировали в Москве
          </Text>
        </View>
        {data.map(value => (
          <View key={value} style={styles.imageCont}>
            <Image source={value.image} style={styles.image} />
          </View>
        ))}
        <View>
          <Text style={styles.text}>
            Как отмечают эксперты, рубль лишь немного меняется к доллару и евро
            на старте торгов после существенного укрепления по итогам пятницы.
            Как отмечают эксперты, рубль лишь немного меняется к доллару и евро
            на старте торгов после существенного укрепления по итогам пятницы.
          </Text>
        </View>
        <Comment
          title="Никита Казначеев"
          text="Евро на старте торгов после существенного укрепления по итогам"
          date="13 марта 7:12"
          positive="23"
          negative="8"
        />
      </ScrollView>
      {/* <View style={styles.bottomView}>
      <Text style={styles.textStyle}>Bottom View</Text>
    </View> */}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
    // paddingBottom: 36,
  },
  top: {
    paddingVertical: 24,
    paddingHorizontal: 20
  },
  title: {
    fontFamily: family.regular,
    ...sizes.primary,
    color: colors.black,
    fontWeight: "500"
  },
  author: {
    paddingBottom: 8,
    fontFamily: family.regular,
    ...sizes.subText,
    color: colors.mountainMist
  },
  text: {
    fontFamily: family.regular,
    ...sizes.subTitle,
    color: colors.tundora,
    paddingVertical: 24,
    paddingHorizontal: 20
  },
  imageCont: {
    paddingBottom: 10
  },
  image: {
    resizeMode: "cover",
    width: "100%",
    height: 288
  }
  // bottomView: {
  //   width: "100%",
  //   height: 50,
  //   backgroundColor: "#EE5407",
  //   justifyContent: "center",
  //   alignItems: "center",
  //   position: "absolute",
  //   bottom: 0,
  //   marginTop: 50
  // },
  // textStyle: {
  //   color: "#fff",
  //   fontSize: 18
  // }
});

export default Journal;
