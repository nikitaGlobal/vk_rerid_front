import React, {
  useRef,
  useState,
  useCallback,
  useEffect,
  useMemo
} from "react";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import { connect, useDispatch, useSelector } from "react-redux";
import { useNavigationParam } from "react-navigation-hooks";
import ImagePicker from "react-native-image-crop-picker";
import Toast from "react-native-easy-toast";
import _ from "lodash";
import Icon from "react-native-vector-icons/MaterialIcons";
import { showMessage } from "react-native-flash-message";
import { removeErrorMessages } from "../../actions/error";
import { getAllCategories } from "../../actions/category";
import useHandleChange from "../../hooks/handleChange";
import { uploadAttachments } from "../../actions/file";
import { family, colors } from "../../utils/view";
import Loading from "../../components/Loading";
import Button from "../../components/Button";
import Select from "../../components/Select";
import { logOut } from "../../actions/auth";
import Input from "../../components/Input";
import {
  createEdition,
  removeEdition,
  getEdition,
  updateEdition
} from "../../actions/edition";
// import api from "../../../api";
import { Img } from "../../components/Custom";

function JournalDetails({
  file,
  isLoading,
  uploadAttachmentsAction,
  getAllCategoriesAction,
  categories,
  isCategoryLoading,
  error,
  removeErrorMessagesAction,
  createEditionAction,
  edition,
  removeEditionAction,
  navigation
}) {
  const scroll = useRef(null);
  const descriptionRef = useRef(null);
  const toast = useRef(null);
  const pubId = useNavigationParam("pubId");
  const isEditionLoading = useSelector(({ edition }) => edition.isLoading);

  const name = useHandleChange("");
  const description = useHandleChange("");
  const [image, setImage] = useState("");
  const [category, setCategory] = useState("");
  const [categoryError, setCategoryError] = useState("");
  const [canAddUsers, setCanAddUser] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    getAllCategoriesAction();
    if (pubId) {
      dispatch(getEdition(pubId)).then(res => {
        name.onChange({
          targetValue: res.name,
          type: "min-length",
          length: 2,
          errMessage: "Имя должно содержать не менее 2 символов"
        });
        description.onChange({
          targetValue: res.description,
          type: "min-length",
          length: 2,
          errMessage: "Имя должно содержать не менее 2 символов"
        });
        setCategory(res.catId);
        setImage(res.avatar);
      });
    }

    return () => {
      removeEditionAction();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, getAllCategoriesAction, pubId, removeEditionAction]);
  useEffect(() => {
    if (!_.isEmpty(error)) {
      showMessage({
        message: error.message,
        type: "danger"
      });
      removeErrorMessagesAction();
    }
  }, [error, removeErrorMessagesAction]);

  const handleSubmit = useCallback(() => {
    if (!category) {
      return setCategoryError("Пожалуйста выберите категорию");
    } else {
      setCategoryError("");
    }
    const isValid =
      name.validate({
        targetValue: name.value,
        type: "min-length",
        length: 2,
        errMessage: "Имя должно содержать не менее 2 символов"
      }) &&
      description.validate({
        targetValue: description.value,
        type: "min-length",
        length: 2,
        errMessage: "Имя должно содержать не менее 2 символов"
      }) &&
      category;

    if (isValid) {
      const data = {
        name: name.value,
        catId: category
      };
      if (!_.isEmpty(file)) {
        data.avatar = file[0].filename;
      }
      if (description.value) {
        data.description = description.value;
      }

      if (pubId) {
        data.id = pubId;
        dispatch(updateEdition(data)).then(() => {
          toast.current.show("Издательство успешно обнавлено");
        });
      } else {
        createEditionAction(data).then(() => {
          setCanAddUser(true);
          toast.current.show("Издательство успешно сохранено");
        });
      }
    }
  }, [category, createEditionAction, description, dispatch, file, name, pubId]);

  const scrollToInput = useCallback(reactNode => {
    scroll.current.scrollToFocusedInput(reactNode);
  }, []);

  const handleImagePress = useCallback(() => {
    ImagePicker.openPicker({
      width: 100,
      height: 100,
      cropping: true,
      cropperCircleOverlay: true,
      compressImageQuality: 0.8,
      cropperToolbarTitle: "Move and Scale",
      useFrontCamera: true
    }).then(imageValue => {
      uploadAttachmentsAction(imageValue).then(res => {
        setImage(res[0].filename);
      });
    });
  }, [uploadAttachmentsAction]);

  const handleSelect = useCallback(value => {
    if (value) {
      setCategoryError("");
    }
    setCategory(value);
  }, []);

  const categoriesData = useMemo(() => {
    return categories.map(item => ({ value: item.id, label: item.name }));
  }, [categories]);

  const handleAdd = useCallback(() => {
    navigation.navigate("Users", { pubId: edition?.id });
  }, [edition, navigation]);

  if (isCategoryLoading || isEditionLoading) {
    return <Loading />;
  }

  return (
    <>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollCont}
        keyboardDismissMode="none"
        keyboardShouldPersistTaps="handled"
        ref={scroll}
      >
        <View style={styles.container}>
          <View style={styles.content}>
            <TouchableOpacity
              style={styles.imageButton}
              onPress={handleImagePress}
            >
              {_.isEmpty(image) ? (
                <View style={[styles.imageBg, styles.placeholder]}>
                  <Text style={styles.placeholderText}>
                    {name.value[0] ? name.value[0].toUpperCase() : ""}
                  </Text>
                </View>
              ) : (
                <Img image={image} url style={styles.imageBg} />
              )}
              <Icon
                name="camera-alt"
                size={28}
                color={colors.amethystSmoke}
                style={styles.cameraIcon}
              />
            </TouchableOpacity>
          </View>
          <View>
            <Select
              label="Категория"
              options={categoriesData}
              value={category}
              onChangeText={handleSelect}
              error={categoryError}
            />
            <Input
              scrollToInput={scrollToInput}
              label="Название издания"
              blurOnSubmit={false}
              returnKeyType="next"
              onSubmitEditing={() => descriptionRef.current.focus()}
              value={name.value}
              onChangeText={text =>
                name.onChange({
                  targetValue: text,
                  type: "min-length",
                  length: 2,
                  errMessage: "Имя должно содержать не менее 2 символов"
                })
              }
              error={name.errorMessage}
            />
            <Input
              scrollToInput={scrollToInput}
              label="Об издании"
              returnKeyType="next"
              onSubmitEditing={handleSubmit}
              inputRef={descriptionRef}
              value={description.value}
              onChangeText={text => {
                description.onChange({
                  targetValue: text,
                  type: "min-length",
                  length: 2,
                  errMessage: "Имя должно содержать не менее 2 символов"
                });
              }}
              error={description.errorMessage}
            />
          </View>
        </View>
        <View style={styles.footer}>
          {canAddUsers ? (
            <Button
              onPress={handleAdd}
              title="Добавить сотрудников"
              container={{
                backgroundColor: colors.prim,
                width: 240,
                marginTop: 24
              }}
              style={{ color: colors.wineBerry }}
            />
          ) : (
            <Button
              onPress={handleSubmit}
              title="Сохранить"
              container={{ backgroundColor: colors.tapestry, width: "100%" }}
              style={{ color: colors.white }}
              isLoading={isLoading}
            />
          )}
        </View>
      </KeyboardAwareScrollView>
      <Toast ref={toast} position="bottom" positionValue={30} opacity={0.8} />
    </>
  );
}

const styles = StyleSheet.create({
  scrollCont: {
    backgroundColor: colors.white,
    flexGrow: 1,
    paddingHorizontal: 19
  },
  content: {
    alignItems: "center"
  },
  imageButton: {
    marginBottom: 24,
    flexDirection: "row",
    alignItems: "flex-end"
  },
  footer: {
    alignItems: "center",
    paddingBottom: 20
  },
  imageBg: {
    width: 96,
    height: 96,
    borderRadius: 48
  },
  placeholder: {
    backgroundColor: colors.tapestry,
    alignItems: "center",
    justifyContent: "center"
  },
  placeholderText: {
    color: colors.white,
    fontSize: 40,
    fontFamily: family.bold
  },
  cameraIcon: {
    marginLeft: 9
  }
});

const mapStateToProps = state => ({
  isLoading: state.user.isLoading,
  isCategoryLoading: state.category.isLoading,
  categories: state.category.categories,
  file: state.file.file,
  edition: state.edition.edition,
  error: state.edition.error
});

const mapDispatchToProps = dispatch => ({
  logOutAction: () => {
    dispatch(logOut());
  },
  uploadAttachmentsAction: data => {
    return dispatch(uploadAttachments(data));
  },
  getAllCategoriesAction: () => {
    dispatch(getAllCategories());
  },
  createEditionAction: data => {
    return dispatch(createEdition(data));
  },
  removeEditionAction: () => {
    dispatch(removeEdition());
  },
  removeErrorMessagesAction: () => {
    dispatch(removeErrorMessages());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JournalDetails);
