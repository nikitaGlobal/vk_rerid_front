import React, { useRef, useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Icon from "react-native-vector-icons/MaterialIcons";
import ImagePicker from "react-native-image-crop-picker";
import {
  View,
  StyleSheet,
  Text,
  Alert,
  Image,
  TouchableOpacity
} from "react-native";
import _ from "lodash";
import Input from "../../components/Input";
import Button from "../../components/Button";
import useHandleChange from "../../hooks/handleChange";
import PubWrap, { PubItems, PubTitle } from "../../components/Publications";
import { logOut } from "../../actions/auth";
import { uploadAttachments } from "../../actions/file";
import { editProfile } from "../../actions/user";
import { family, colors, sizes } from "../../utils/view";
import placeholder from "../../../assets/img/avatar-placeholder.png";
import api from "../../../api";
import { useNavigation } from "react-navigation-hooks";
import { EDITOR } from "../../utils/editionRoles";

function EditProfile() {
  const { navigate } = useNavigation();
  const scroll = useRef(null);
  const dispatch = useDispatch();
  const { isLoading, user, file } = useSelector(({ user, file }) => ({
    isLoading: user.isLoading,
    user: user.user,
    file: file.file
  }));

  const descriptionRef = useRef(null);
  const name = useHandleChange(user.user ? user.user.name : "");
  const description = useHandleChange(user.user.description);
  const [image, setImage] = useState({});

  const handleSubmit = useCallback(() => {
    const isValid = name.validate({ filed: name, type: "length", length: 2 });

    if (isValid) {
      const data = {
        name: name.value
      };
      if (!_.isEmpty(file)) {
        data.avatar = file[0].filename;
      }
      if (description.value) {
        data.description = description.value;
      }

      dispatch(editProfile(data));
    }
  }, [description.value, dispatch, file, name]);

  const scrollToInput = useCallback(() => {
    // scroll.current.scrollToFocusedInput(reactNode);
  }, []);

  const handleImagePress = useCallback(() => {
    ImagePicker.openPicker({
      width: 100,
      height: 100,
      cropping: true,
      cropperCircleOverlay: true,
      compressImageQuality: 0.8,
      cropperToolbarTitle: "Move and Scale",
      useFrontCamera: true
    }).then(imageValue => {
      setImage(imageValue);
      dispatch(uploadAttachments(imageValue));
    });
  }, [dispatch]);

  const handleLogOut = useCallback(() => {
    Alert.alert("Предупреждение", "Вы уверены что хотите выйти?", [
      {
        text: "Нет"
      },
      {
        text: "Да",
        onPress: () => {
          navigate("Launch");
          dispatch(logOut());
        }
      }
    ]);
  }, [dispatch, navigate]);

  if (_.isEmpty(user)) return null;
  return (
    <KeyboardAwareScrollView
      contentContainerStyle={styles.scrollCont}
      keyboardDismissMode="none"
      keyboardShouldPersistTaps="handled"
      showsVerticalScrollIndicator={false}
      ref={scroll}
    >
      <View style={styles.content}>
        <TouchableOpacity style={styles.imageButton} onPress={handleImagePress}>
          <Image
            source={
              _.isEmpty(image)
                ? user.user.avatar
                  ? { uri: `${api.url}upload/${user.user.avatar}` }
                  : placeholder
                : { uri: image.sourceURL }
            }
            style={styles.image}
          />
          <Icon
            name="camera-alt"
            size={28}
            color={colors.amethystSmoke}
            style={styles.cameraIcon}
          />
        </TouchableOpacity>
      </View>
      <View>
        <Input
          scrollToInput={scrollToInput}
          label="Имя Фамилия"
          blurOnSubmit={false}
          returnKeyType="next"
          onSubmitEditing={() => descriptionRef.current.focus()}
          value={name.value}
          onChangeText={text =>
            name.onChange({
              targetValue: text,
              type: "length",
              length: 2,
              errMessage: "Имя должно содержать не менее 2 символов"
            })
          }
          error={name.errorMessage}
        />
        <Input
          scrollToInput={scrollToInput}
          label="О себе"
          blurOnSubmit={false}
          returnKeyType="next"
          onSubmitEditing={handleSubmit}
          inputRef={descriptionRef}
          value={description.value}
          onChangeText={text => {
            description.onChange({
              targetValue: text
            });
          }}
          error={description.errorMessage}
        />
        <Input
          scrollToInput={scrollToInput}
          label="Email"
          editable={false}
          value={user.user.email}
        />
      </View>
      {user.user.publications && (
        <>
          <PubTitle title="Мои издания" />
          <PubWrap>
            {user.user.publications.map(item => {
              return (
                <TouchableOpacity
                  key={item.id}
                  style={styles.pubWrap}
                  onPress={() =>
                    navigate("Articles", { pubId: item.id, role: EDITOR })
                  }
                >
                  <PubItems name={item.name} image={item.avatar} />
                </TouchableOpacity>
              );
            })}
          </PubWrap>
        </>
      )}
      <View style={styles.footer}>
        <Button
          onPress={handleSubmit}
          title="Сохранить изменения"
          container={{ backgroundColor: colors.tapestry, width: "100%" }}
          style={{ color: colors.white }}
          isLoading={isLoading}
        />
        <Button
          onPress={() => navigate("ChangePassword")}
          title="Поменять пароль"
          container={{
            backgroundColor: colors.prim,
            width: 240,
            marginTop: 24
          }}
          style={{ color: colors.wineBerry }}
        />
        <TouchableOpacity style={styles.logout} onPress={handleLogOut}>
          <Text style={styles.logOut}>ВЫХОД</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  scrollCont: {
    paddingHorizontal: 19,
    paddingVertical: 10,
    flexGrow: 1
  },
  content: {
    alignItems: "center"
  },
  logOut: {
    ...sizes.medium,
    fontFamily: family.medium,
    color: colors.wineBerry
  },
  imageButton: {
    marginBottom: 24,
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-end"
  },
  footer: {
    alignItems: "center",
    paddingTop: 20
  },
  logout: {
    marginTop: 24
  },
  image: {
    width: 96,
    height: 96,
    borderRadius: 48
  },
  cameraIcon: {
    marginLeft: 9
  },
  pubWrap: {
    paddingVertical: 6
  }
});

export default EditProfile;
