import React, { useCallback, useEffect, useState } from "react";
import { StyleSheet, FlatList, View, Text, RefreshControl, AsyncStorage } from "react-native";
import { colors, family } from "../../utils/view";
import CommentItem from "../../components/CommentItem";
import { useDispatch, useSelector } from "react-redux";
import { DateTime } from "luxon";
import avatar from "../../../assets/img/avatar-placeholder.png";
import { useNavigation } from "react-navigation-hooks";
import { getAllNotifications } from "../../actions/notification";
import Loading from "../../components/Loading";
import api from "../../../api";
import editionRoles from "../../utils/editionRoles";

function Notifications() {
  const { navigate } = useNavigation();
  const dispatch = useDispatch();
  const { notifications, userId } = useSelector(({ notification, user }) => ({
    notifications: notification.notifications,
    userId: user?.user?.user?.id
  }));
  const [isLoading, setLoading] = useState(false);
  const [isRefreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await dispatch(getAllNotifications());
    setRefreshing(false);
  }, [dispatch]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await dispatch(getAllNotifications()).then(length => {
        AsyncStorage.setItem("nottifications", length || 0);
        dispatch({ type: "SET_VIEWED_COUNT", payload: length || 0 });
      });
      setLoading(false);
    })();
  }, [dispatch]);

  const handleAnswer = useCallback(
    id => {
      navigate("NotificationDetails", { id });
    },
    [navigate]
  );

  const renderItem = useCallback(
    ({ item }) => {
      if (!item) {
        return null;
      }
      if (!item.info || !item.send) {
        return null;
      }
      if (!item.info.publication) {
        return null;
      }
      let title = `Приглашает Вас в издательство ${
        item.info.publication.name
      } на вакансию ${editionRoles[item.info.role]}`;

      if (item.send.id === userId) {
        title = `Вы пригласили ${item.invited_user?.name} в издательство ${
          item.info.publication.name
        } на вакансию ${editionRoles[item.info.role]}`;
      }
      return (
        <CommentItem
          name={item.send.name}
          title={title}
          text={item.comment}
          date={DateTime.fromISO(item.createdAt).toLocaleString(
            DateTime.DATE_MED
          )}
          image={
            item.send.avatar
              ? { uri: `${api.url}upload/${item.send.avatar}` }
              : avatar
          }
          onAnswer={() => handleAnswer(item.id)}
          canAnswer
        />
      );
    },
    [handleAnswer, userId]
  );

  const renderEmptyComponent = useCallback(
    () => (
      <View style={styles.empty}>
        <Text style={styles.text}>Пока нет уведомлений</Text>
      </View>
    ),
    []
  );

  if (!notifications || (notifications && !notifications.length)) {
    return renderEmptyComponent();
  }
  if (isLoading) {
    return <Loading />;
  }

  return (
    <FlatList
      data={notifications.sort((a, b) => {
        a = new Date(a.createdAt);
        b = new Date(b.createdAt);
        return a > b ? -1 : a < b ? 1 : 0;
      })}
      style={styles.scroll}
      contentContainerStyle={styles.container}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      ListEmptyComponent={renderEmptyComponent}
      refreshControl={
        <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
      }
    />
  );
}

const styles = StyleSheet.create({
  scroll: {
    flex: 1,
    backgroundColor: colors.white
  },
  empty: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: "center",
    justifyContent: "center",
    padding: 15
  },
  container: {
    flexGrow: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 20,
    paddingBottom: 20
  },
  text: {
    textAlign: "center",
    fontFamily: family.regular,
    fontSize: 18
  }
});

export default Notifications;
