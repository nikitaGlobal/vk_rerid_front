/* eslint-disable react/display-name */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useCallback, useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  View,
  StyleSheet,
  Dimensions,
  FlatList,
  Text,
  RefreshControl
} from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import Icon from "react-native-vector-icons/Octicons";
import Ripple from "react-native-material-ripple";
import { DateTime } from "luxon";
import { useNavigationParam, useNavigation } from "react-navigation-hooks";
import { colors, family } from "../../utils/view";
import Loading from "../../components/Loading";
import {
  JOURNALIST,
  ARTICLE_DRAFT,
  ARTICLE_ACTIVE,
  ARTICLE_WAITING
} from "../../utils/editionRoles";
import { getAllArticles } from "../../actions/article";
import ArticlesWrap, { ArticleItem } from "../../components/Articles";
import CkeckerWrap, { Checker } from "../../components/Checker";

const RenderArticeles = ({ type }) => {
  const dispatch = useDispatch();
  const role = useNavigationParam("role");
  const pubId = useNavigationParam("pubId");
  const { navigate } = useNavigation();
  const articles = useSelector(({ article }) => article.articles);

  const [isLoading, setLoading] = useState(false);
  const [isRefreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await dispatch(getAllArticles(pubId, type));
    setRefreshing(false);
  }, [dispatch, pubId, type]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await dispatch(getAllArticles(pubId, type));
      setLoading(false);
    })();
  }, [dispatch, pubId, type]);

  const renderItem = useCallback(
    ({ item }) => (
      <Ripple onPress={() => handleNavigate(item.id)}>
        <ArticlesWrap>
          <ArticleItem
            text={item.description}
            date={DateTime.fromISO(item.createdAt).toLocaleString(
              DateTime.DATE_MED
            )}
          />
          <CkeckerWrap>
            <Checker text="Корректор" active={item.CORRECTOR} />
            <Checker text="Юрист" active={item.LAWYER} />
            <Checker text="Редактор" active={item.EDITOR} />
          </CkeckerWrap>
        </ArticlesWrap>
      </Ripple>
    ),
    [handleNavigate]
  );

  const handleNavigate = useCallback(
    id => {
      navigate("ArticleDetails", { id, role, pubId });
    },
    [navigate, pubId, role]
  );

  const renderEmptyComponent = useCallback(
    () => (
      <View style={styles.empty}>
        <Text style={styles.text}>Пока нет статей</Text>
      </View>
    ),
    []
  );

  const data = useMemo(() => {
    const dattatype = type || "all";

    if (articles[dattatype]) return articles[dattatype];

    return [];
  }, [articles, type]);

  if (isLoading) return <Loading />;
  return (
    <FlatList
      data={data.sort((a, b) => {
        a = new Date(a.createdAt);
        b = new Date(b.createdAt);
        return a > b ? -1 : a < b ? 1 : 0;
      })}
      contentContainerStyle={styles.container}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      ListEmptyComponent={renderEmptyComponent}
      refreshControl={
        <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
      }
    />
  );
};

function Articles() {
  const pubId = useNavigationParam("pubId");
  const role = useNavigationParam("role");
  const { navigate } = useNavigation();

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: "all", title: "Все" },
    { key: "drafts", title: "Черновики" },
    { key: "waiting", title: "Не опубликованные" },
    { key: "published", title: "Опубликованные" }
  ]);

  const getLabelText = useCallback(({ route }) => route.title, []);

  const renderTabBar = useCallback(
    props => (
      <TabBar
        {...props}
        style={styles.tabBar}
        labelStyle={styles.labelStyle}
        activeColor={colors.wineBerry}
        inactiveColor={colors.dustyGray}
        tabStyle={styles.tabStyle}
        contentContainerStyle={styles.tabContainer}
        indicatorStyle={styles.indicatorStyle}
        getLabelText={getLabelText}
        scrollEnabled
      />
    ),
    [getLabelText]
  );

  const addArticle = useCallback(() => {
    navigate("CreateArticle", {
      pubId
    });
  }, [navigate, pubId]);

  const renderScreens = useMemo(() => {
    return SceneMap({
      all: () => <RenderArticeles />,
      drafts: () => (
        <RenderArticeles type={ARTICLE_DRAFT} key={ARTICLE_DRAFT} />
      ),
      waiting: () => (
        <RenderArticeles type={ARTICLE_WAITING} key={ARTICLE_WAITING} />
      ),
      published: () => (
        <RenderArticeles type={ARTICLE_ACTIVE} key={ARTICLE_ACTIVE} />
      )
    });
  }, []);

  return (
    <View style={styles.root}>
      {role === JOURNALIST ? (
        <TabView
          renderTabBar={renderTabBar}
          navigationState={{ index, routes }}
          renderScene={renderScreens}
          onIndexChange={index => setIndex(index)}
          initialLayout={{ width: Dimensions.get("window").width }}
          lazy
        />
      ) : (
        <RenderArticeles />
      )}
      {role === JOURNALIST && (
        <Ripple style={styles.addArticle} onPress={addArticle}>
          <Icon name="plus" size={32} color={colors.white} />
        </Ripple>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: colors.white
  },
  tabBar: {
    backgroundColor: colors.white,
    elevation: 0,
    shadowOpacity: 0,
    shadowColor: colors.white
  },
  labelStyle: {
    fontFamily: family.medium,
    fontSize: 18
  },
  indicatorStyle: {
    backgroundColor: colors.wineBerry,
    borderRadius: 5,
    height: 3
  },
  tabStyle: {
    paddingHorizontal: 8,
    width: "auto"
  },
  addArticle: {
    borderRadius: 34,
    position: "absolute",
    right: 19,
    bottom: 19,
    backgroundColor: colors.tapestry,
    width: 68,
    height: 68,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    paddingTop: 5
  },
  container: {
    flexGrow: 1,
    backgroundColor: colors.white
  },
  empty: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: "center",
    justifyContent: "center",
    padding: 15
  },
  text: {
    textAlign: "center",
    fontFamily: family.regular,
    fontSize: 18
  }
});

export default Articles;
