import React, { useEffect, useCallback, useState, memo } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  FlatList,
  RefreshControl
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { getAllFavorites } from "../../actions/favorite";
import { ListItem } from "../../components/Favorite";
import { colors, family } from "../../utils/view";
import favoriteImg from "../../../assets/img/empty_favorite.png";
import { DateTime } from "luxon";
import { useNavigation } from "react-navigation-hooks";
import Loading from "../../components/Loading";

function ListItemComponent({
  id,
  name,
  avatar,
  navigate,
  description,
  createdAt
}) {
  const handlePress = useCallback(() => {
    navigate("PopularItem", { title: name, id });
  }, [id, name, navigate]);
  return (
    <>
      <ListItem
        key={id}
        id={id}
        onPress={handlePress}
        name={name}
        image={avatar}
        role={DateTime.fromISO(createdAt).toLocaleString(DateTime.DATE_MED)}
        border
      />
      <Text style={styles.description}>{description}</Text>
    </>
  );
}

export const Items = memo(ListItemComponent);

function Favorite() {
  const { navigate } = useNavigation();
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState(false);
  const [isRefreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await dispatch(getAllFavorites());
    setRefreshing(false);
  }, [dispatch]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await dispatch(getAllFavorites());
      setLoading(false);
    })();
  }, [dispatch]);

  const favorite = useSelector(({ favorite }) => favorite.favorite);
  const renderItem = useCallback(
    ({ item }) => (
      <Items
        id={item.pubId}
        navigate={navigate}
        name={item.publication.name}
        avatar={item.publication.avatar}
        createdAt={item.createdAt}
        description={item.publication.description}
      />
    ),
    [navigate]
  );

  const renderEmptyComponent = useCallback(
    () => (
      <View style={styles.container}>
        <Image source={favoriteImg} style={styles.image} />
        <Text style={styles.text}>Добавьте интересные издания в избранное</Text>
      </View>
    ),
    []
  );

  if (isLoading) {
    return <Loading />;
  }
  return (
    <FlatList
      data={favorite}
      contentContainerStyle={styles.content}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      ListEmptyComponent={renderEmptyComponent}
      refreshControl={
        <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
      }
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: "center",
    justifyContent: "center",
    padding: 15
  },
  content: {
    backgroundColor: colors.white,
    flexGrow: 1,
    paddingHorizontal: 20
  },
  image: {
    width: 100,
    height: 100,
    marginBottom: 25
  },
  text: {
    textAlign: "center",
    fontFamily: family.regular,
    fontSize: 18
  }
});

export default Favorite;
