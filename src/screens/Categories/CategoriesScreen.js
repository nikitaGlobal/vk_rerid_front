import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { StyleSheet, FlatList, RefreshControl, View, Text } from "react-native";
import { colors, family } from "../../utils/view";
import CategoriesItem from "../../components/Categories";
import { getAllCategories } from "../../actions/category";
import Loading from "../../components/Loading";

function Categories() {
  const { categories } = useSelector(({ category }) => ({
    categories: category.categories
  }));
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState(false);
  const [isRefreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await dispatch(getAllCategories());
    setRefreshing(false);
  }, [dispatch]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await dispatch(getAllCategories());
      setLoading(false);
    })();
  }, [dispatch]);

  const renderItem = useCallback(
    ({ item }) => (
      <CategoriesItem
        name={item.name}
        key={item.id}
        image={item.avatar}
        id={item.id}
        editions={item.pob ? item.pob.Count : 0}
        hideMenu
      />
    ),
    []
  );

  const renderEmptyComponent = useCallback(
    () => (
      <View style={styles.empty}>
        <Text style={styles.text}>Пока нет категорий</Text>
      </View>
    ),
    []
  );

  if (isLoading) {
    return <Loading />;
  }
  return (
    <FlatList
      data={categories}
      contentContainerStyle={styles.container}
      renderItem={renderItem}
      keyExtractor={item => item.id}
      ListEmptyComponent={renderEmptyComponent}
      refreshControl={
        <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
      }
    />
  );
}

const styles = StyleSheet.create({
  empty: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: "center",
    justifyContent: "center",
    padding: 15
  },
  container: {
    flexGrow: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 20
  },
  text: {
    textAlign: "center",
    fontFamily: family.regular,
    fontSize: 18
  }
});

export default Categories;
