/* eslint-disable react-native/no-color-literals */
import React, {
  useRef,
  useCallback,
  useEffect,
  useMemo,
  useState
} from "react";
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  Keyboard,
  FlatList
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import _ from "lodash";
import Toast from "react-native-easy-toast";
import { showMessage } from "react-native-flash-message";
import { DateTime } from "luxon";
import AudioTrack from "../../components/AudioTrack";
import { colors, family } from "../../utils/view";
import Loading from "../../components/Loading";
import Button from "../../components/Button";
import { removeErrorMessages } from "../../actions/error";
import {
  getArticleById,
  changeArticleStatus,
  updateTyoeArticle
} from "../../actions/article";
import api from "../../../api";
import { useNavigationParam, useNavigation } from "react-navigation-hooks";
import CkeckerWrap, { Checker } from "../../components/Checker";
import avatar from "../../../assets/img/avatar-placeholder.png";

import {
  JOURNALIST,
  CORRECTOR,
  LAWYER,
  EDITOR,
  ARTICLE_ACTIVE,
  ARTICLE_WAITING
} from "../../utils/editionRoles";
import { Img } from "../../components/Custom";
import CommentInput from "../../components/CommentInput";
import CommentItem from "../../components/CommentItem";
import { socket } from "../../utils/socket";
import { GET_ARTICLE, INCREASE_COMMENTS } from "../../constants/article";

function ArticleDetails() {
  const scrollRef = useRef(null);
  const user = useSelector(({ user }) => user.user.user);
  const article = useSelector(({ article }) => article);
  const [comment, setComment] = useState("");
  const role = useNavigationParam("role");
  const id = useNavigationParam("id");
  const pubId = useNavigationParam("pubId");
  const toast = useRef(null);
  const dispatch = useDispatch();
  const { navigate, goBack } = useNavigation();

  useEffect(() => {
    dispatch(getArticleById(id)).then(data => {
      if (socket) {
        socket.emit("join", { id, name: data.name });
      }
    });
  }, [dispatch, id]);

  const getNewComment = useCallback(
    data => {
      const newData = {
        ...article.article,
        comment: article.article.comment
          ? [...article.article.comment, data]
          : [data]
      };

      dispatch({ type: GET_ARTICLE, payload: newData });
      dispatch({ type: INCREASE_COMMENTS, payload: id });
      setTimeout(() => {
        scrollRef.current.scrollToEnd({ animated: true });
      }, 300);
    },
    [article.article, dispatch, id]
  );

  useEffect(() => {
    if (socket) {
      socket.on("comment", getNewComment);
    }
  }, [getNewComment]);

  useEffect(() => {
    if (!_.isEmpty(article.error)) {
      showMessage({
        message: article.error.message,
        type: "danger"
      });
      dispatch(removeErrorMessages());
    }
  }, [article.error, dispatch]);

  const handleApprove = useCallback(() => {
    dispatch(changeArticleStatus(article.article.id, pubId)).then(() => {
      showMessage({
        message: "Данные успешно обновлены",
        type: "success"
      });
    });
  }, [article.article.id, dispatch, pubId]);

  const handleEdit = useCallback(() => {
    navigate("EditArticles", { id });
  }, [id, navigate]);

  const handleDelete = useCallback(() => {
    // approve
  }, []);

  const handleReject = useCallback(() => {
    dispatch(updateTyoeArticle(id, pubId, ARTICLE_WAITING));
    goBack();
  }, [dispatch, goBack, id, pubId]);

  const handleSubmit = useCallback(() => {
    dispatch(updateTyoeArticle(id, pubId, ARTICLE_ACTIVE));
  }, [dispatch, id, pubId]);

  const handleCommentSubmit = useCallback(() => {
    setComment("");
    const commentData = { comment, articleId: id, userId: user.id };
    socket.emit("comment", commentData);
    const newData = {
      ...article.article,
      comment: article.article.comment
        ? [
            ...article.article.comment,
            {
              ...commentData,
              user,
              createdAt: DateTime.local().toISO(),
              id: DateTime.local().toISO()
            }
          ]
        : [
            {
              ...commentData,
              user,
              createdAt: DateTime.local().toISO(),
              id: DateTime.local().toISO()
            }
          ]
    };

    dispatch({ type: GET_ARTICLE, payload: newData });
    dispatch({ type: INCREASE_COMMENTS, payload: id });
    Keyboard.dismiss();
    setTimeout(() => {
      scrollRef.current.scrollToEnd({ animated: true });
    }, 300);
  }, [article.article, comment, dispatch, id, user]);

  const renderButtons = useMemo(() => {
    switch (role) {
      case JOURNALIST:
        return (
          <>
            <Button
              onPress={handleEdit}
              title="Редактировать"
              container={{
                backgroundColor: colors.tapestry,
                paddingHorizontal: 40
              }}
              style={{ color: colors.white }}
            />
            <Button
              onPress={handleDelete}
              title="Удалить"
              container={{
                backgroundColor: colors.prim,
                marginTop: 24,
                paddingHorizontal: 30
              }}
              style={{ color: colors.wineBerry }}
            />
          </>
        );
      case CORRECTOR:
        return (
          <>
            <Button
              onPress={handleApprove}
              title="Утвердить"
              container={{
                backgroundColor: colors.tapestry,
                paddingHorizontal: 40
              }}
              style={{ color: colors.white }}
            />
            <Button
              onPress={handleEdit}
              title="Редактировать"
              container={{
                backgroundColor: colors.prim,
                marginTop: 24,
                paddingHorizontal: 30
              }}
              style={{ color: colors.wineBerry }}
            />
            <Button
              onPress={handleReject}
              title="Отправить на доработку"
              container={{
                backgroundColor: colors.prim,
                marginTop: 24,
                paddingHorizontal: 30
              }}
              style={{ color: colors.wineBerry }}
            />
          </>
        );
      case LAWYER:
        return (
          <>
            <Button
              onPress={handleApprove}
              title="Утвердить"
              container={{
                backgroundColor: colors.tapestry,
                paddingHorizontal: 40
              }}
              style={{ color: colors.white }}
            />
            <Button
              onPress={handleReject}
              title="Отправить на доработку"
              container={{
                backgroundColor: colors.prim,
                marginTop: 24,
                paddingHorizontal: 30
              }}
              style={{ color: colors.wineBerry }}
            />
          </>
        );
      case EDITOR:
        return (
          <>
            <Button
              onPress={handleSubmit}
              title="Опубликовать"
              container={{
                backgroundColor: colors.tapestry,
                paddingHorizontal: 40
              }}
              style={{ color: colors.white }}
            />
            <Button
              onPress={handleReject}
              title="Отправить на доработку"
              container={{
                backgroundColor: colors.prim,
                marginTop: 24,
                paddingHorizontal: 30
              }}
              style={{ color: colors.wineBerry }}
            />
          </>
        );
      default:
        return null;
    }
  }, [
    handleApprove,
    handleDelete,
    handleEdit,
    handleReject,
    handleSubmit,
    role
  ]);

  const renderComment = useCallback(
    ({ item }) => (
      <CommentItem
        name={item.user.name}
        text={item.comment}
        date={DateTime.fromISO(item.createdAt).toLocaleString(
          DateTime.DATETIME_MED
        )}
        image={
          item.user.avatar
            ? { uri: `${api.url}upload/${item.user.avatar}` }
            : avatar
        }
      />
    ),
    []
  );

  if (article.isLoading || _.isEmpty(article.article)) {
    return <Loading />;
  }
  return (
    <View style={styles.root}>
      <ScrollView contentContainerStyle={styles.scrollCont} ref={scrollRef}>
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.title}>{article.article.name}</Text>
            <Text style={styles.text}>{article.article.description}</Text>
          </View>
          {article.article.articleAudios?.map((item, key) => (
            <View key={key.toString()} style={styles.imgWrap}>
              <AudioTrack audio={`${api.url}upload/audio/${item.filename}`} />
            </View>
          ))}
          {article.article.articleImages?.map(item => (
            <View key={item.filename} style={styles.imgWrap}>
              <Img image={item.filename} style={styles.image} url />
            </View>
          ))}
          {role === JOURNALIST && (
            <CkeckerWrap>
              <Checker text="Корректор" active={article.article.CORRECTOR} />
              <Checker text="Юрист" active={article.article.LAWYER} />
              <Checker text="Редактор" active={article.article.EDITOR} />
            </CkeckerWrap>
          )}
        </View>
        <View style={styles.footer}>{renderButtons}</View>
        {!role && !!article.article.comment && (
          <FlatList
            data={article.article.comment?.sort((a, b) => {
              a = new Date(a.createdAt);
              b = new Date(b.createdAt);
              return a < b ? -1 : a > b ? 1 : 0;
            })}
            keyExtractor={comment => comment.id}
            renderItem={renderComment}
            contentContainerStyle={styles.flatList}
          />
        )}
      </ScrollView>
      {!role && (
        <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={105}>
          <CommentInput
            onChange={text => setComment(text)}
            handleSubmit={handleCommentSubmit}
            value={comment}
          />
        </KeyboardAvoidingView>
      )}
      <Toast ref={toast} position="bottom" positionValue={250} opacity={0.8} />
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: colors.white
  },
  container: {
    flex: 1
  },
  scrollCont: {
    flexGrow: 1,
    paddingHorizontal: 19,
    paddingBottom: 15
  },
  image: {
    height: 231,
    width: Dimensions.get("window").width - 40,
    resizeMode: "cover"
  },
  imgWrap: {
    flexDirection: "row",
    alignItems: "flex-end",
    paddingBottom: 10
  },
  title: {
    fontFamily: family.medium,
    fontSize: 24,
    color: colors.black,
    lineHeight: 36,
    letterSpacing: 0.24,
    marginBottom: 9
  },
  text: {
    fontSize: 18,
    lineHeight: 28.8,
    marginBottom: 24,
    fontFamily: family.regular
  },
  footer: {
    alignItems: "center",
    paddingBottom: 20,
    paddingTop: 15
  },
  flatList: {
    flexGrow: 1
  }
});

export default ArticleDetails;
