import io from "socket.io-client";
import api from "../../api";

let socket = null;
const connectToSocket = (id, name, callback = () => false) => {
  socket = io.connect(api.base_url, {
    reconnection: true,
    reconnectionDelay: 5000,
    reconnectionDelayMax: 5000,
    reconnectionAttempts: Infinity,
    transports: ["websocket"]
  });
  if (socket) {
    socket.on("disconnect", reason => {
      // console.log("reason", reason);
    });
    socket.on("connect", () => {
      // console.log("connect");
      socket.emit("join", { id, name });
      callback();
    });
    socket.on("error", error => {
      // console.log("error", error);
    });
  }
};

const disconnectFromSocket = () => {
  socket.disconnect();
};

export { socket, connectToSocket, disconnectFromSocket };
