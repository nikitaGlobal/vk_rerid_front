export default {
  "2": "Редактура",
  "4": "Написание статей",
  "8": "Корректура",
  "16": "Юридическая проверка"
};

export const EDITOR = "2";
export const JOURNALIST = "4";
export const CORRECTOR = "8";
export const LAWYER = "16";

export const ARTICLE_WAITING = "1";
export const ARTICLE_DRAFT = "2";
export const ARTICLE_ACTIVE = "3";

export const PENDING_STATE = "1";
export const CONFIRMED_STATE = "2";
export const REJECTED_STATE = "3";
