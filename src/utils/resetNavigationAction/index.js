import { StackActions, NavigationActions } from "react-navigation";

export default (navigation, route, params) => {
  const NavigationOption = StackActions.reset({
    index: 0,
    key: null,
    actions: [
      NavigationActions.navigate({
        routeName: route,
        params: params
      })
    ]
  });
  navigation.dispatch(NavigationOption);
};
