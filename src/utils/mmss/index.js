export default function(number) {
  const date = new Date(null);
  date.setSeconds(number);
  return date.toISOString().substr(14, 5);
}

export function mmssmm(number) {
  const date = new Date(null);
  date.setMilliseconds(number);
  return date.toISOString().substr(14, 8);
}
