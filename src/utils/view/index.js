const family = {
  black: "Roboto-Black",
  blackItalic: "Roboto-BlackItalic",
  bold: "Roboto-Bold",
  boldItalic: "Roboto-BoldItalic",
  italic: "Roboto-Italic",
  light: "Roboto-Light",
  lightItalic: "Roboto-LightItalic",
  medium: "Roboto-Medium",
  mediumItalic: "Roboto-MediumItalic",
  regular: "Roboto-Regular",
  thin: "Roboto-Thin",
  thinItalic: "Roboto-ThinItalic"
};

const sizes = {
  headerTitle: {
    fontSize: 25.2
  },
  primary: {
    fontSize: 24,
    lineHeight: 36
  },
  title: {
    fontSize: 22.8
  },
  list: {
    fontSize: 18.4
  },
  input: {
    fontSize: 19.2,
    lineHeight: 48
  },
  subTitle: {
    fontSize: 18,
    lineHeight: 28.8
  },
  button: {
    fontSize: 17,
    lineHeight: 26
  },
  medium: {
    fontSize: 16.8
  },
  regular: {
    fontSize: 15,
    lineHeight: 24
  },
  label: {
    fontSize: 14.4,
    lineHeight: 30
  },
  subText: {
    fontSize: 14.4,
    lineHeight: 18
  },
  warning: {
    fontSize: 12,
    lineHeight: 18
  },
  error: {
    fontSize: 12
  }
};

const colors = {
  black: "#000000",
  white: "#FFFFFF",
  wineBerry: "#461943",
  prim: "#EFE1ED",
  mountainMist: "#939296",
  tapestry: "#B35B9B",
  error: "#EC011C",
  tundora: "#444444",
  amethystSmoke: "#9F94A9",
  mercury: "#E3E3E3",
  dustyGray: "#9B9B9B",
  gallery: "#EBEBEB",
  cancel: "#ED5D68"
};

export { family, sizes, colors };
