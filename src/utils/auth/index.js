import {AsyncStorage} from "react-native";

export const KEY = "token";
export const onSignIn = () => AsyncStorage.setItem(KEY, "true");
export const setStorage = (name, data) => AsyncStorage.setItem(name, data);
export const logOut = () => AsyncStorage.clear();

export const isSignedIn = () =>
  new Promise((resolve, reject) => {
    AsyncStorage.getItem(KEY)
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
