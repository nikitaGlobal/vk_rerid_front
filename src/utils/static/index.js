const types = [
  {
    value: "saloonCar",
    name: "Sedan/Sports"
  },
  {
    value: "suv",
    name: "SUV"
  },
  {
    value: "mpv",
    name: "MPV"
  }
  // {
  //   value: 'mpvMoreSixSeats',
  //   name: 'MPV more 6 seats'
  // }
];

const parkingData = [
  {
    value: "outdoor",
    name: "Open-Air"
  },
  {
    value: "indoor",
    name: "Sheltered"
  }
];

const paymentMethod = [
  {
    value: "prePaid",
    name: "Prepaid wosh"
  },
  {
    value: "card",
    name: "Card"
  }
];

const priceObj = {
  saloonCar: 13,
  suv: 15,
  mpv: 14,
  mpvMoreSixSeats: 19
};

export { types, parkingData, paymentMethod, priceObj };
