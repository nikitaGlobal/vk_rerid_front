import { createStore, applyMiddleware, compose } from "redux";
import thunkMiddleware from "redux-thunk";
// import { createLogger } from "redux-logger";
import AppReducer from "../reducers";
import { RootNavigator } from "../navigation/navigation";

const navReducer = (state, action) => {
  const newState = RootNavigator().router.getStateForAction(action, state);
  return newState || state;
};
// const loggerMiddleware = createLogger({
//   predicate: () => __DEV__
// });
const configureStore = initialState => {
  const enhancer = compose(applyMiddleware(thunkMiddleware));
  return createStore(AppReducer(navReducer), initialState, enhancer);
};
export default configureStore;
