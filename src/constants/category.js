export const CATEGORY_LOADING = "CATEGORY_LOADING";
export const GET_ALL_CATEGORIES = "GET_ALL_CATEGORIES";
export const CATEGORY_ERROR = "CATEGORY_ERROR";
