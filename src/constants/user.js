export const USER_LOADING = "USER_LOADING";
export const USER_DATA_SUCCESS = "USER_DATA_SUCCESS";
export const USER_ERROR = "USER_ERROR";
export const GET_ALL_USERS = "GET_ALL_USERS";
export const SET_USER_BY_ID = "SET_USER_BY_ID";
