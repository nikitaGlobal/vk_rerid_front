import React, { memo, useCallback } from "react";
import { StyleSheet, View, Text } from "react-native";
import RNPickerSelect from "react-native-picker-select";
import { colors, family, sizes } from "../../utils/view";

function Select({ label, options, onChangeText, value, error }) {
  const handleChange = useCallback(
    event => {
      onChangeText(event);
    },
    [onChangeText]
  );

  return (
    <View style={styles.root}>
      <View style={styles.input}>
        <RNPickerSelect
          placeholder={{ label }}
          style={{
            inputIOS: {
              borderBottomColor: error ? colors.error : colors.wineBerry,
              ...styles.inputStyle
            },
            inputAndroid: {
              borderBottomColor: error ? colors.error : colors.wineBerry,
              ...styles.inputStyle
            }
          }}
          items={options}
          onValueChange={handleChange}
          value={value}
        />
      </View>
      <Text style={styles.error}>{error}</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  root: {
    paddingTop: 15
  },
  inputStyle: {
    height: 46,
    textAlignVertical: "bottom",
    borderBottomWidth: 2,
    fontFamily: family.regular,
    padding: 10,
    color: colors.black
  },
  error: {
    paddingLeft: 10,
    fontFamily: family.light,
    ...sizes.error,
    color: colors.error,
    paddingTop: 3,
    height: 18
  }
});

Select.defaultProps = {
  error: "",
  value: ""
};

export default memo(Select);
