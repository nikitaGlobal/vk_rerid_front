import React, { forwardRef } from "react";
import { StyleSheet, TextInput, View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { colors, family } from "../../utils/view";

const CommentInput = forwardRef(({ onChange, handleSubmit, value }, ref) => {
  return (
    <View style={styles.comment}>
      <TextInput
        ref={ref}
        style={styles.input}
        onChangeText={onChange}
        onSubmitEditing={handleSubmit}
        placeholder="Напишите комментарий..."
        returnKeyType="done"
        value={value}
      />
      <TouchableOpacity style={styles.button} onPress={handleSubmit}>
        <Icon name="send" size={25} color={colors.wineBerry} />
      </TouchableOpacity>
    </View>
  );
});

const styles = StyleSheet.create({
  comment: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    backgroundColor: colors.white,
    borderTopColor: colors.mercury,
    borderTopWidth: 1,
    position: "relative"
  },
  input: {
    paddingVertical: 8,
    paddingHorizontal: 12,
    paddingRight: 30,
    borderRadius: 30,
    backgroundColor: colors.gallery,
    height: 40,
    width: "100%",
    fontFamily: family.regular
  },
  button: {
    position: "absolute",
    top: 12,
    right: 22
  }
});

CommentInput.displayName = "CommentInput";

export default CommentInput;
