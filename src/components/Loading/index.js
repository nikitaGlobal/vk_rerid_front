import React, { memo } from "react";
import { View, StyleSheet, ActivityIndicator } from "react-native";
import { colors } from "../../utils/view";

function Loading() {
  return (
    <View style={styles.container}>
      <ActivityIndicator size="large" color={colors.wineBerry} />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default memo(Loading);
