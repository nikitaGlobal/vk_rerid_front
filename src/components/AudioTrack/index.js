import React, {
  memo,
  useCallback,
  useState,
  useEffect,
  useMemo,
  useRef
} from "react";
import { View, StyleSheet, TouchableOpacity, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Sound from "react-native-sound";
import { showMessage } from "react-native-flash-message";
import { colors } from "../../utils/view";
import mmss from "../../utils/mmss";

function AudioTrack({ audio }) {
  const [isPlay, setPlay] = useState(false);
  const sound = useRef(null);
  const timer = useRef(null);
  const [currentPositionSec, setCurrentPositionSec] = useState(0);
  const [currentDurationSec, setCurrentDurationSec] = useState(0);
  const [duration, setDuration] = useState("00:00");

  useEffect(() => {
    sound.current = new Sound(audio, "", error => {
      if (error) {
        showMessage({
          message: "Произошла ошибка, пожалуйста повторите попытку",
          type: "danger"
        });
      }
    });

    getAudioDuration();
  }, [audio, getAudioDuration]);

  const getAudioDuration = useCallback(() => {
    const dur = sound.current.getDuration();

    if (dur > -1) {
      setDuration(mmss(dur));
      setCurrentDurationSec(dur / 1000);
    } else {
      setTimeout(() => {
        getAudioDuration();
      }, 10);
    }
  }, []);

  const onPausePlay = useCallback(async () => {
    setPlay(false);
    clearInterval(timer.current);
    sound.current.pause();
  }, []);

  const onStartPlay = useCallback(async () => {
    timer.current = setInterval(() => {
      sound.current.getCurrentTime(seconds => {
        setCurrentPositionSec(seconds / 1000);
      });
    }, 1);
    setPlay(true);
    setTimeout(() => {
      sound.current.play(() => {
        setPlay(false);
        clearInterval(timer.current);
      });
    }, 100);
  }, []);

  const playWidth = useMemo(
    () => (currentPositionSec * 100) / currentDurationSec,
    [currentDurationSec, currentPositionSec]
  );

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={isPlay ? onPausePlay : onStartPlay}>
        <Icon
          name={isPlay ? "pause-circle" : "play-circle"}
          size={30}
          color={colors.amethystSmoke}
        />
      </TouchableOpacity>
      <View style={styles.trackContainer}>
        <View style={styles.root}>
          <View style={[styles.track, { width: `${playWidth}%` }]} />
        </View>
      </View>
      <Text>{duration}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1
  },
  trackContainer: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 14
  },
  root: {
    height: 3.6,
    backgroundColor: colors.mercury
  },
  track: {
    height: 3.6,
    backgroundColor: colors.amethystSmoke
  }
});

AudioTrack.defaultProps = {
  width: 0,
  audio: ""
};

export default memo(AudioTrack);
