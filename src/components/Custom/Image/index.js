import React, { useState, memo } from "react";
import { View, Image, ActivityIndicator, StyleSheet } from "react-native";
import api from "../../../../api/index.json";
import placeholder from "../../../../assets/img/avatar-placeholder.png";

const Img = ({ image, url, ...rest }) => {
  // const [loading, setLoading] = useState(true);

  return (
    <View style={styles.root}>
      <Image
        // onLoadStart={() => setLoading(true)}
        // onLoadEnd={() => setLoading(false)}
        source={
          image
            ? url
              ? { uri: `${api.url}upload/${image}`, cache: "force-cache" }
              : image
            : placeholder
        }
        {...rest}
      />
      {/* {loading ? <ActivityIndicator style={styles.indicator} /> : null} */}
    </View>
  );
};

Img.defaultProps = {
  url: false,
  image: null
};

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    justifyContent: "center",
    position: "relative"
  },
  indicator: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: "auto"
  }
});

export default memo(Img);
