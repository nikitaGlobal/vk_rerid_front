import React from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableWithoutFeedback
} from "react-native";
import { colors, family, sizes } from "../../utils/view";

function Feedback({ image, count, disabled, onPress }) {
  return (
    <TouchableWithoutFeedback disabled={disabled} onPress={onPress}>
      <View style={styles.container}>
        <Image source={image} style={styles.image} />
        <View style={styles.textBlock}>
          <Text style={styles.text}>{count}</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

Feedback.defaultProps = {
  image: "",
  count: 0,
  onPress: () => null
};
const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginRight: 10
  },
  text: {
    ...sizes.label,
    color: colors.mountainMist,
    fontFamily: family.regular
  },
  textBlock: {
    paddingRight: 5
  },
  image: {
    width: 20,
    height: 20,
    margin: 4,
    resizeMode: "contain"
  }
});
export default Feedback;
