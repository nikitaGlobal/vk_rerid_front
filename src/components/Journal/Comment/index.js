import React, { memo } from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";
import { colors, family, sizes } from "../../../utils/view";
import like from "../../../../assets/img/icons/like.png";
import dislike from "../../../../assets/img/icons/dislike.png";
import image from "../../../../assets/img/avatar-placeholder.png";

function Comment({ title, text, date, positive, negative }) {
  return (
    <View style={styles.root}>
      <View>
        <Image
          // source={{ uri: data.img, cache: 'force-cache' }}
          source={image}
          style={styles.image}
        />
      </View>
      <View style={styles.content}>
        <View style={styles.textCont}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.text}>{text}</Text>
        </View>
        <View style={styles.row}>
          <View style={styles.top}>
            <Text style={styles.textStyle}>{date}</Text>
            <TouchableOpacity style={styles.answer}>
              <Text style={styles.textStyle}>Ответить</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.middle}>
            <View style={styles.action}>
              <TouchableOpacity style={styles.row}>
                <Text style={[styles.textStyle, styles.right]}>{positive}</Text>
                <Image source={like} style={styles.smallImage} />
              </TouchableOpacity>
            </View>
            <View style={styles.bottom}>
              <TouchableOpacity style={styles.row}>
                <Text style={[styles.textStyle, styles.right]}>{negative}</Text>
                <Image source={dislike} style={styles.smallImage} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

Comment.defaultProps = {
  img: "",
  title: "",
  text: "",
  date: "",
  positive: 0,
  negative: 0
};
const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: colors.mercury,
    paddingVertical: 14,
    paddingHorizontal: 20
  },
  title: {
    fontFamily: family.regular,
    color: colors.black,
    ...sizes.list,
    fontWeight: "500",
    paddingBottom: 8
  },
  text: {
    ...sizes.list,
    color: colors.tundora,
    fontFamily: family.regular
  },
  textStyle: {
    color: colors.mountainMist,
    ...sizes.subText,
    fontFamily: family.regular
  },
  image: {
    resizeMode: "contain",
    width: 48,
    height: 48
  },
  smallImage: {
    resizeMode: "contain",
    width: 25,
    height: 25
  },
  row: { flexDirection: "row", alignItems: "center" },
  content: { flex: 1, paddingLeft: 15 },
  textCont: { paddingBottom: 30 },
  top: { flexDirection: "row", paddingRight: 10 },
  middle: { flexDirection: "row", alignItems: "center", flex: 1 },
  answer: { paddingLeft: 15 },
  action: { paddingLeft: 10 },
  right: { paddingRight: 5 },
  bottom: { paddingLeft: 15 }
});

export default memo(Comment);
