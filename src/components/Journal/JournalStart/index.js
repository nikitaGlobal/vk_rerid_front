import React, { useEffect, memo, useState } from "react";
import { StyleSheet, View, Text, AsyncStorage } from "react-native";
import { colors, family, sizes } from "../../../utils/view";
import img from "../../../../assets/img/culture.png";

import Button from "../../Button";
import { Img } from "../../Custom";

function JournalStart({ handleAddPress }) {
  const [guest, setGuestValue] = useState("");

  useEffect(() => {
    AsyncStorage.getItem("token").then(token => {
      if (token === "guest") {
        setGuestValue("guest");
      }
    });
  }, []);
  return (
    <View style={styles.root}>
      <View style={styles.content}>
        <View style={styles.imgBlock}>
          <Img image={img} style={styles.image} />
        </View>
        <Text style={styles.text}>
          У вас пока нет своего издания, но вы можете его создать прямо сейчас
        </Text>
      </View>
      {guest !== "guest" && (
        <View style={styles.footer}>
          <Button
            onPress={handleAddPress}
            title="Создать"
            container={{ backgroundColor: colors.tapestry, width: 162 }}
            style={{ color: colors.white }}
          />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  imgBlock: {
    width: 96,
    height: 96,
    borderRadius: 48,
    backgroundColor: colors.prim,
    justifyContent: "center",
    alignItems: "center"
  },
  root: {
    flex: 1,
    paddingHorizontal: 15
  },
  content: {
    marginTop: 88,
    alignItems: "center"
  },
  text: {
    fontFamily: family.regular,
    color: colors.tundora,
    ...sizes.subTitle,
    paddingBottom: 40,
    paddingTop: 32
  },
  image: {
    resizeMode: "contain",
    width: 57,
    height: 57
  },
  footer: { alignItems: "center", paddingTop: 40 }
});

export default memo(JournalStart);
