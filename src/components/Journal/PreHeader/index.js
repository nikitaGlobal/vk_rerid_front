import React, { memo } from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import { colors, sizes, family } from "../../../utils/view";
import img from "../../../../assets/img/avatar-placeholder.png";
import more from "../../../../assets/img/more.png";

function PreHeader({ name, date }) {
  return (
    <View style={styles.root}>
      <View style={styles.content}>
        <Image source={img} style={styles.leftImage} />
        <View style={styles.text}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.date}>{date}</Text>
        </View>
      </View>
      <View>
        <TouchableOpacity>
          <Image source={more} style={styles.rightImage} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

PreHeader.defaultProps = {
  name: "",
  date: ""
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: colors.white,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    paddingTop: 20,
    paddingHorizontal: 20
  },
  content: { flexDirection: "row", alignItems: "center" },
  text: { paddingLeft: 20 },
  name: {
    ...sizes.subTitle,
    color: colors.black,
    fontFamily: family.medium
  },
  date: {
    ...sizes.label,
    color: colors.mountainMist,
    fontFamily: family.regular
  },
  leftImage: {
    resizeMode: "contain",
    width: 48,
    height: 48,
    borderRadius: 24
  },
  rightImage: {
    resizeMode: "contain",
    width: 29,
    height: 29
  }
});
export default memo(PreHeader);
