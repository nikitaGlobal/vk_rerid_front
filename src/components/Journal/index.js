import JournalItem from "./JournalItem";
import SectionHeader from "./SectionHeader";
import JournalStart from "./JournalStart";
import PreHeader from "./PreHeader";
import Comment from "./Comment";

export default JournalItem;
export { SectionHeader, JournalStart, PreHeader, Comment };
