import React, { memo, useCallback } from "react";
import { StyleSheet, View, Text } from "react-native";
import Ripple from "react-native-material-ripple";
import { colors, family, sizes } from "../../../utils/view";
import { Img } from "../../Custom";
import { useNavigation } from "react-navigation-hooks";

function JournalItem({ name, color, count, image, id, role }) {
  const { navigate } = useNavigation();
  const navigateToArticles = useCallback(
    () => navigate("Articles", { pubId: id, role }),
    [id, navigate, role]
  );
  return (
    <Ripple style={styles.root} onPress={navigateToArticles}>
      <View style={styles.imageCont}>
        {image ? (
          <Img image={image} style={styles.image} url />
        ) : (
          <View style={styles.placeholder}>
            <Text style={styles.placeholderText}>{name[0].toUpperCase()}</Text>
          </View>
        )}
        <View>
          <Text style={styles.name}>{name}</Text>
        </View>
      </View>
      {!!count && (
        <View style={[styles.countStyle, { backgroundColor: color }]}>
          <Text style={styles.text}>{count}</Text>
        </View>
      )}
    </Ripple>
  );
}

JournalItem.defaultProps = {
  image: "",
  name: "",
  count: 0,
  color: colors.prim,
  id: "",
  role: "2"
};
const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: colors.mercury,
    paddingVertical: 13
  },
  imageCont: {
    flexDirection: "row",
    alignItems: "center"
  },
  name: {
    fontFamily: family.regular,
    color: colors.black,
    ...sizes.list
  },
  countStyle: {
    borderRadius: 14,
    width: 29,
    height: 29,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  image: {
    resizeMode: "contain",
    width: 48,
    height: 48,
    marginRight: 19,
    borderRadius: 24
  },
  placeholder: {
    width: 48,
    height: 48,
    marginRight: 19,
    backgroundColor: colors.tapestry,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 24
  },
  placeholderText: {
    color: colors.white,
    fontSize: 20,
    fontFamily: family.bold
  },
  text: {
    color: colors.white,
    fontFamily: family.bold
  }
});

export default memo(JournalItem);
