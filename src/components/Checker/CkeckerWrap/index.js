import React from "react";
import { View, StyleSheet } from "react-native";
import { colors } from "../../../utils/view";

function CheckerWrap({ children }) {
  return <View style={styles.container}>{children}</View>;
}
CheckerWrap.defaultProps = {
  children: null,
  customStyle: {}
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    borderBottomColor: colors.prim,
    borderBottomWidth: 1,
    borderTopColor: colors.prim,
    borderTopWidth: 1,
    paddingVertical: 11
  }
});

export default CheckerWrap;
