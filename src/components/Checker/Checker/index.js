import React from "react";
import { StyleSheet, Text } from "react-native";
import Ripple from "react-native-material-ripple";

import { family, colors } from "../../../utils/view";
import { Img } from "../../Custom";

import activeImage from "../../../../assets/img/active.png";
import question from "../../../../assets/img/question.png";

function Checker({ active, text, onPress }) {
  return (
    <Ripple style={styles.checkWrap} onPress={onPress}>
      {active ? (
        <Img source={activeImage} style={styles.image} />
      ) : (
        <Img source={question} style={styles.image} />
      )}
      <Text style={styles.text}>{text}</Text>
    </Ripple>
  );
}

Checker.defaultProps = {
  text: "",
  active: false,
  onPress: () => false
};
const styles = StyleSheet.create({
  checkWrap: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15
  },
  image: {
    width: 19,
    height: 19,
    resizeMode: "contain"
  },
  text: {
    marginLeft: 15,
    fontSize: 12,
    fontFamily: family.regular,
    color: colors.mountainMist
  }
});
export default Checker;
