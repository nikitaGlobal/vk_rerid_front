/* eslint-disable react-native/no-raw-text */
import React, { useCallback, useRef } from "react";
import Menu, { MenuItem } from "react-native-material-menu";
import Ripple from "react-native-material-ripple";
import { StyleSheet, View, Text } from "react-native";
import { useNavigation } from "react-navigation-hooks";
import { Img } from "../../Custom";
import { colors, family, sizes } from "../../../utils/view";
import more from "../../../../assets/img/more.png";

function CategoriesItem({ name, editions, news, image, id, hideMenu }) {
  const { navigate } = useNavigation();
  const menuRef = useRef(null);
  const showMenu = useCallback(() => {
    menuRef.current.show();
  }, []);
  const handleViewProfile = useCallback(() => {
    menuRef.current.hide();
  }, []);

  return (
    <>
      <Ripple
        style={styles.content}
        onPress={() => navigate("Popular", { title: name, id })}
      >
        <View style={styles.container}>
          <View style={styles.imgWrap}>
            <Img image={image} url style={styles.image} />
          </View>
          <View>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.subName}>
              {editions} изданий, {news} новостей в день{" "}
            </Text>
          </View>
        </View>
      </Ripple>
      {!hideMenu && (
        <View>
          <Menu
            ref={menuRef}
            style={styles.menu}
            button={
              <Ripple style={styles.btn} onPress={showMenu}>
                <Img image={more} style={styles.settings} />
              </Ripple>
            }
          >
            <MenuItem onPress={handleViewProfile}>Профиль</MenuItem>
            {/* <MenuItem onPress={handleViewProfile}>См. Профиль</MenuItem> */}
          </Menu>
        </View>
      )}
    </>
  );
}

CategoriesItem.defaultProps = {
  news: 0,
  editions: 0,
  image: "",
  hideMenu: false
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row"
  },
  content: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 10,
    borderBottomColor: colors.mercury,
    borderBottomWidth: 1,
    paddingBottom: 10,
    paddingTop: 10
  },
  imgWrap: {
    paddingRight: 20
  },
  image: {
    width: 29,
    height: 29,
    resizeMode: "contain"
  },
  name: {
    ...sizes.list,
    color: colors.black,
    fontFamily: family.regular
  },
  subName: {
    ...sizes.label,
    color: colors.mountainMist,
    fontFamily: family.regular
  },
  settings: {
    resizeMode: "contain",
    width: 29,
    height: 29
  },
  btn: {
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden"
  },
  menu: {
    marginTop: 40
  }
});
export default CategoriesItem;
