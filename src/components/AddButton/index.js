import React, { memo } from "react";
import { View, Text, StyleSheet } from "react-native";
import Ripple from "react-native-material-ripple";
import { family, sizes, colors } from "../../utils/view";

function AddButton({ container, onPress, style, title }) {
  return (
    <Ripple style={[styles.button, container]} onPress={onPress}>
      <View style={styles.plus}>
        <Text style={styles.plusText}>+</Text>
      </View>
      <Text style={[styles.text, style]}>{title}</Text>
    </Ripple>
  );
}

const styles = StyleSheet.create({
  button: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 5,
    marginHorizontal: 15
  },
  plus: {
    width: 48,
    height: 48,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.prim,
    borderRadius: 24,
    marginRight: 19
  },
  plusText: {
    fontFamily: family.light,
    fontSize: 15
  },
  text: {
    fontFamily: family.regular,
    ...sizes.button
  }
});

AddButton.defaultProps = {
  container: {},
  style: {}
};

export default memo(AddButton);
