import React, { useCallback } from "react";
import { StyleSheet, View, Alert, Text, TouchableOpacity } from "react-native";
import { useDispatch } from "react-redux";
import { addOrRemoveFavorite } from "../../../actions/favorite";
import { colors, family } from "../../../utils/view";
import { Img } from "../../../components/Custom";

import active_start from "../../../../assets/img/active_start.png";

function ListItem({ name, image, border, onPress, id }) {
  const dispatch = useDispatch();
  const handlClick = useCallback(() => {
    Alert.alert(
      "Предупреждение",
      "Вы уверены что хотите удалить издание из избранных?",
      [
        { text: "Отмена" },
        {
          text: "ОК",
          onPress: () => dispatch(addOrRemoveFavorite({ pubId: id }))
        }
      ],
      { cancelable: false }
    );
  }, [dispatch, id]);
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.container,
        // eslint-disable-next-line react-native/no-inline-styles
        {
          borderBottomWidth: border ? 0 : 1
        }
      ]}
    >
      <View style={styles.infoWrap}>
        {image ? (
          <Img url image={image} style={styles.image} />
        ) : (
          <View style={styles.placeholder}>
            <Text style={styles.placeholderText}>{name[0].toUpperCase()}</Text>
          </View>
        )}
        <View style={styles.info}>
          <Text style={styles.name}>{name}</Text>
        </View>
      </View>
      <TouchableOpacity onPress={handlClick}>
        <Img image={active_start} style={styles.imgIcon} />
      </TouchableOpacity>
    </TouchableOpacity>
  );
}

ListItem.defaultProps = {
  image: "",
  border: false,
  onPress: () => false
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomColor: colors.mercury,
    paddingVertical: 10
  },
  infoWrap: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    resizeMode: "contain",
    width: 48,
    height: 48,
    marginRight: 19,
    borderRadius: 24
  },
  placeholder: {
    width: 48,
    height: 48,
    marginRight: 19,
    backgroundColor: colors.tapestry,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 24
  },
  placeholderText: {
    color: colors.white,
    fontSize: 20,
    fontFamily: family.bold
  },
  name: {
    fontSize: 14,
    fontFamily: family.bold,
    paddingBottom: 3
  },
  imgIcon: {
    width: 29,
    height: 29,
    resizeMode: "contain"
  }
});
export default ListItem;
