import React, { memo } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import Ripple from "react-native-material-ripple";
import { colors, family, sizes } from "../../utils/view";

function ListItem({ onPress, icon, text, count, disabled }) {
  return (
    <Ripple style={styles.root} onPress={onPress} disabled={disabled}>
      <View style={styles.left}>
        <Image source={icon} style={styles.image} />
        <Text style={styles.text}>{text}</Text>
      </View>
      {!!count && (
        <View style={styles.countCont}>
          <Text style={styles.count}>{count}</Text>
        </View>
      )}
    </Ripple>
  );
}
ListItem.defaultProps = {
  count: 0,
  icon: -1
};

const styles = StyleSheet.create({
  root: {
    paddingBottom: 12,
    paddingTop: 16,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: colors.mercury
  },
  left: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    width: 29,
    height: 29,
    resizeMode: "contain",
    marginRight: 19
  },
  text: {
    fontFamily: family.regular,
    color: colors.black,
    ...sizes.list
  },
  countCont: {
    width: 30,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 15,
    backgroundColor: colors.tapestry
  },
  count: {
    ...sizes.medium,
    fontFamily: family.bold,
    color: colors.white
  }
});

export default memo(ListItem);
