import React, { memo } from "react";
import { View, Image, Text, TouchableOpacity, StyleSheet } from "react-native";
import { family, colors } from "../../utils/view";

function CommentItem({
  image,
  name,
  title,
  text,
  date,
  children,
  canAnswer,
  onAnswer
}) {
  return (
    <>
      <View style={styles.root}>
        <View>
          <Image source={image} style={styles.image} />
        </View>
        <View style={styles.content}>
          <View style={styles.textes}>
            <Text style={styles.bold}>{name}</Text>
            {!!title && <Text style={styles.bold}>{title}</Text>}
            <Text style={styles.text}>{text}</Text>
          </View>
          <View style={styles.foot}>
            <Text style={styles.light}>{date}</Text>
            {canAnswer && (
              <TouchableOpacity onPress={onAnswer}>
                <Text style={styles.light}>Ответить</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
      <View style={styles.children}>{children}</View>
    </>
  );
}

const styles = StyleSheet.create({
  root: {
    flexDirection: "row",
    paddingTop: 20
  },
  image: {
    width: 48,
    height: 48,
    borderRadius: 24,
    resizeMode: "cover"
  },
  content: {
    flex: 1,
    paddingLeft: 19
  },
  textes: {
    paddingBottom: 30
  },
  bold: {
    fontFamily: family.medium,
    fontSize: 18,
    color: colors.black,
    paddingBottom: 3
  },
  text: {
    fontSize: 18,
    fontFamily: family.regular,
    color: colors.tundora
  },
  foot: {
    flexDirection: "row"
  },
  light: {
    paddingRight: 23,
    fontSize: 14.4,
    color: colors.mountainMist,
    fontFamily: family.regular
  },
  children: {
    paddingLeft: 67
  }
});

CommentItem.defaultProps = {
  title: "",
  children: null,
  canAnswer: false,
  onAnswer: () => false
};

export default memo(CommentItem);
