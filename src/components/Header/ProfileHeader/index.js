import React, { useEffect, useState, memo } from "react";
import { StyleSheet, View, TouchableOpacity, Text, AsyncStorage } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { colors, sizes, family } from "../../../utils/view";

function ProfileHeader({ navigation, title }) {
  const [guest, setGuestValue] = useState("loading");

  useEffect(() => {
    AsyncStorage.getItem("token").then(token => {
      if (token === "guest") {
        setGuestValue("guest");
      } else {
        setGuestValue("user");
      }
    });
  }, []);

  return (
    <View style={styles.root}>
      <View style={styles.titleCont}>
        <Text style={styles.title}>{title}</Text>
      </View>
      {guest === "user" && (
        <TouchableOpacity
          style={styles.icon}
          onPress={() => navigation.navigate("EditProfile")}
        >
          <Icon name="md-settings" size={29} color={colors.amethystSmoke} />
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: colors.white,
    height: 67,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  titleCont: { paddingLeft: 38 },
  title: {
    ...sizes.headerTitle,
    color: colors.wineBerry,
    fontFamily: family.medium
  },
  icon: {
    paddingVertical: 19,
    paddingHorizontal: 19,
    height: 67
  }
});

export default memo(ProfileHeader);
