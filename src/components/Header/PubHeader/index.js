/* eslint-disable react-native/no-raw-text */
import React, { useRef, useCallback, useMemo, memo } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Alert } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import { colors } from "../../../utils/view";
import { useNavigation } from "react-navigation-hooks";
import Menu, { MenuItem } from "react-native-material-menu";
import Ripple from "react-native-material-ripple";
import more from "../../../../assets/img/more.png";
import { EDITOR } from "../../../utils/editionRoles";

function PubHeader({ navigation }) {
  const { navigate } = useNavigation();
  const pubId = useMemo(() => navigation.getParam("pubId"), [navigation]);
  const role = useMemo(() => navigation.getParam("role"), [navigation]);

  const menuRef = useRef(null);

  const showMenu = useCallback(() => {
    menuRef.current.show();
  }, []);
  const handleNavigate = useCallback(() => {
    navigate("Users", {
      pubId
    });
    menuRef.current.hide();
  }, [navigate, pubId]);

  const handleEdit = useCallback(() => {
    navigate("JournalDetails", { pubId });
  }, [navigate, pubId]);

  return (
    <View style={styles.root}>
      <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
        <Icon name="arrow-left" size={29} />
      </TouchableOpacity>
      {role === EDITOR && (
        <Menu
          ref={menuRef}
          style={styles.menu}
          button={
            <Ripple style={styles.btn} onPress={showMenu}>
              <Image source={more} style={styles.settings} />
            </Ripple>
          }
        >
          <>
            <MenuItem onPress={handleEdit}>Редактировать</MenuItem>
            <MenuItem onPress={handleNavigate}>Добавить сотрудников</MenuItem>
          </>
        </Menu>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: colors.white,
    height: 67,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  back: {
    paddingVertical: 19,
    paddingHorizontal: 15,
    height: 67
  },
  settings: {
    resizeMode: "contain",
    width: 29,
    height: 29
  },
  btn: {
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden"
  },
  menu: {
    marginTop: 40
  }
});

export default memo(PubHeader);
