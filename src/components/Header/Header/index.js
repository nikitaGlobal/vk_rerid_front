import React, { memo } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import { colors, sizes, family } from "../../../utils/view";

function Header({ route, navigation, back, title }) {
  return (
    <View style={styles.root}>
      {back && (
        <TouchableOpacity
          style={styles.back}
          onPress={() =>
            route ? navigation.navigate(route) : navigation.goBack()
          }
        >
          <Icon name="arrow-left" size={29} />
        </TouchableOpacity>
      )}
      {!!title && (
        <View style={styles.titleCont}>
          <Text style={styles.title} numberOfLines={1}>
            {title}
          </Text>
        </View>
      )}
    </View>
  );
}

Header.defaultProps = {
  route: "",
  title: "",
  back: false
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: colors.white,
    height: 67,
    flexDirection: "row",
    alignItems: "center"
    // justifyContent: "space-between"
  },
  back: {
    paddingVertical: 19,
    paddingHorizontal: 15,
    height: 67
  },
  titleCont: { paddingLeft: 15, flex: 1 },
  title: {
    ...sizes.headerTitle,
    color: colors.wineBerry,
    fontFamily: family.medium
  }
});

export default memo(Header);
