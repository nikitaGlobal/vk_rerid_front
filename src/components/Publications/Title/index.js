import React, { memo } from "react";
import { StyleSheet, View, Text } from "react-native";
import { colors, sizes, family } from "../../../utils/view";

function PubTitle({ title }) {
  return (
    <View style={styles.root}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}
PubTitle.defaultProps = {
  title: ""
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: colors.white,
    flexDirection: "row",
    alignItems: "center"
  },
  title: {
    ...sizes.primary,
    color: colors.black,
    fontFamily: family.medium
  }
});

export default memo(PubTitle);
