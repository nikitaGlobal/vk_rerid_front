import React from "react";
import { View, StyleSheet } from "react-native";

const PubWrap = ({ children }) => {
  return <View style={styles.root}>{children}</View>;
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: "column"
  }
});
export default PubWrap;
