import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Img } from "../../Custom";
import { colors, sizes, family } from "../../../utils/view";

const PubItems = ({ image, name, style }) => {
  return (
    <View style={[styles.root, style]}>
      <View style={styles.imgWrap}>
        <Img image={image} style={styles.image} url />
      </View>
      <View style={styles.textWrap}>
        <Text style={styles.text}>{name}</Text>
      </View>
    </View>
  );
};

PubItems.defaultProps = {
  image: null,
  name: ""
};
const styles = StyleSheet.create({
  root: {
    flexDirection: "row",
    // justifyContent: "center",
    paddingBottom: 2,
    borderBottomColor: colors.mercury,
    borderBottomWidth: 1
  },
  imgWrap: {
    paddingRight: 20
  },
  image: {
    width: 40,
    height: 40,
    resizeMode: "contain",
    borderRadius: 20
  },
  text: {
    color: colors.black,
    fontFamily: family.regular,
    ...sizes.regular
  }
});

export default PubItems;
