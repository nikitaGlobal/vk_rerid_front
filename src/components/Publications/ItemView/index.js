import React, { useState, useCallback } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { useDispatch } from "react-redux";
import { addOrRemoveFavorite } from "../../../actions/favorite";
import { colors, sizes, family } from "../../../utils/view";
import { Img } from "../../Custom";
import star from "../../../../assets/img/star.png";
import active_start from "../../../../assets/img/active_start.png";

function ItemView({ image, name, description, read, id, favoriteItem, style }) {
  const dispatch = useDispatch();
  const [active, setActive] = useState(favoriteItem === id ? true : false);
  const handlClick = useCallback(() => {
    dispatch(addOrRemoveFavorite({ pubId: id })).then(() => {
      setActive(!active);
    });
  }, [active, dispatch, id]);

  return (
    <View style={[styles.root, style]}>
      <View style={styles.wrap}>
        <View style={styles.content}>
          <View style={styles.imgWrap}>
            <Img image={image} style={styles.image} url />
          </View>
          <View style={styles.textWrap}>
            <Text style={styles.text} numberOfLines={1}>
              {name}
            </Text>
            <Text style={styles.role}>Читатели {read}</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.icon} onPress={handlClick}>
          <Img image={active ? active_start : star} style={styles.imgIcon} />
        </TouchableOpacity>
      </View>
      <Text style={styles.desc}>{description}</Text>
    </View>
  );
}

ItemView.defaultProps = {
  image: null,
  name: "",
  read: 0
};
const styles = StyleSheet.create({
  root: {
    flexDirection: "column",
    paddingBottom: 15
  },
  content: {
    flexDirection: "row",
    flex: 1
  },
  wrap: {
    justifyContent: "space-between",
    flexDirection: "row",
    // justifyContent: "center",
    paddingBottom: 19
  },
  imgWrap: {
    paddingRight: 20
  },
  imgIcon: {
    width: 29,
    height: 29,
    resizeMode: "contain"
  },
  image: {
    width: 96,
    height: 96,
    resizeMode: "contain",
    borderRadius: 48
  },
  text: {
    ...sizes.primary,
    color: colors.black,
    fontFamily: family.medium
  },
  desc: {
    ...sizes.subTitle,
    fontFamily: family.regular,
    color: colors.tundora
  },
  role: {
    fontSize: 14,
    color: colors.mountainMist,
    fontFamily: family.regular
  },
  textWrap: {
    flex: 1
  }
});

export default ItemView;
