import React from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";
import { colors, family } from "../../utils/view";
import api from "../../../api";

function ListItemWithMenu({ name, role, image, children, border, onPress }) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.container,
        // eslint-disable-next-line react-native/no-inline-styles
        {
          borderBottomWidth: border ? 0 : 1
        }
      ]}
    >
      <View style={styles.infoWrap}>
        {image ? (
          <Image
            source={{ uri: `${api.url}upload/${image}`, cache: "force-cache" }}
            style={styles.image}
          />
        ) : (
          <View style={styles.placeholder}>
            <Text style={styles.placeholderText}>{name[0].toUpperCase()}</Text>
          </View>
        )}
        <View style={styles.info}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.role}>{role}</Text>
        </View>
      </View>
      <View style={styles.children}>{children}</View>
    </TouchableOpacity>
  );
}

ListItemWithMenu.defaultProps = {
  image: "",
  role: "",
  children: null,
  border: false,
  onPress: () => false
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomColor: colors.mercury,
    paddingVertical: 10
  },
  infoWrap: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    resizeMode: "contain",
    width: 48,
    height: 48,
    marginRight: 19,
    borderRadius: 24
  },
  placeholder: {
    width: 48,
    height: 48,
    marginRight: 19,
    backgroundColor: colors.tapestry,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 24
  },
  placeholderText: {
    color: colors.white,
    fontSize: 20,
    fontFamily: family.bold
  },
  name: {
    fontSize: 14,
    fontFamily: family.bold,
    paddingBottom: 3
  },
  role: {
    fontSize: 12,
    color: colors.mountainMist,
    fontFamily: family.regular
  }
});
export default ListItemWithMenu;
