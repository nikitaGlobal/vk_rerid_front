import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Animated,
  findNodeHandle
} from "react-native";
import { colors, family, sizes } from "../../utils/view";

function FloatingLabelInput({
  label,
  container,
  error,
  inputStyles,
  value,
  scrollToInput,
  inputRef,
  ...rest
}) {
  const animatedIsFocused = useRef(new Animated.Value(0));
  const [isFocused, setFocusValue] = useState(false);

  useEffect(() => {
    Animated.timing(animatedIsFocused.current, {
      toValue: value || isFocused ? 1 : 0,
      duration: 200
    }).start();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused, value]);

  return (
    <View style={[styles.root, container]}>
      <Animated.Text
        style={[
          styles.labelStyle,
          {
            top: animatedIsFocused.current.interpolate({
              inputRange: [0, 1],
              outputRange: [35, 4]
            }),
            fontSize: animatedIsFocused.current.interpolate({
              inputRange: [0, 1],
              outputRange: [14, 12]
            }),
            color: isFocused ? colors.wineBerry : colors.mountainMist
          }
        ]}
      >
        {label}
      </Animated.Text>
      <TextInput
        ref={inputRef}
        underlineColorAndroid="transparent"
        style={[
          styles.input,
          {
            borderBottomColor: error ? colors.error : colors.wineBerry
          },
          inputStyles
        ]}
        onFocus={event => {
          scrollToInput(findNodeHandle(event.target));
          setFocusValue(true);
        }}
        onBlur={() => {
          if (!value) {
            setFocusValue(false);
          }
        }}
        value={value}
        {...rest}
      />
      <Text style={styles.error}>{error}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    paddingTop: 15,
    position: "relative"
  },
  error: {
    paddingLeft: 10,
    fontFamily: family.light,
    ...sizes.error,
    color: colors.error,
    paddingTop: 3,
    height: 18
  },
  input: {
    height: 46,
    textAlignVertical: "bottom",
    borderBottomWidth: 2,
    fontFamily: family.regular,
    padding: 10
  },
  labelStyle: {
    position: "absolute",
    left: 10,
    fontFamily: family.regular
  }
});

FloatingLabelInput.defaultProps = {
  container: {},
  inputStyles: {},
  error: "",
  value: "",
  rest: {},
  scrollToInput: () => false
};

export default FloatingLabelInput;
