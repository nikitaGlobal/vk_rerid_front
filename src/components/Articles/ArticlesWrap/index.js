import React, { memo } from "react";
import { View, StyleSheet } from "react-native";

function ArticleWrap({ children, customStyle }) {
  return <View style={[styles.container, customStyle]}>{children}</View>;
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 10
  }
});

ArticleWrap.defaultProps = {
  children: null,
  customStyle: {}
};
export default memo(ArticleWrap);
