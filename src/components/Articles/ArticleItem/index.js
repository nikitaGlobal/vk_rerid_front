import React, { memo } from "react";
import { View, Text, StyleSheet } from "react-native";
import { family, sizes, colors } from "../../../utils/view";

function ArticleItem({ text, type, date }) {
  return (
    <View style={[styles.container]}>
      <View style={styles.textWrap}>
        <Text style={styles.text}>{text}</Text>
      </View>
      <View style={styles.infoWrap}>
        <Text style={styles.item}>{type}</Text>
        <Text style={styles.item}>{date}</Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  textWrap: {
    paddingVertical: 10
  },
  text: {
    ...sizes.list,
    color: colors.black,
    fontFamily: family.regular
  },
  infoWrap: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 5
  },
  item: {
    ...sizes.label,
    color: colors.mountainMist,
    fontFamily: family.regular
  }
});

ArticleItem.defaultProps = {
  text: "",
  type: "",
  date: ""
};
export default memo(ArticleItem);
