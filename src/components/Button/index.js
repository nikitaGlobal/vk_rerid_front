import React, { memo } from "react";
import { Text, StyleSheet, ActivityIndicator } from "react-native";
import Ripple from "react-native-material-ripple";
import { family, sizes, colors } from "../../utils/view";

function Button({ container, onPress, style, title, isLoading }) {
  return (
    <Ripple
      disabled={isLoading}
      style={[styles.button, container]}
      onPress={onPress}
    >
      <Text style={[styles.text, style]}>{title}</Text>
      {isLoading && (
        <ActivityIndicator
          style={styles.loading}
          color={colors.white}
          size="small"
        />
      )}
    </Ripple>
  );
}

const styles = StyleSheet.create({
  button: {
    paddingVertical: 8,
    borderRadius: 22,
    alignItems: "center",
    overflow: "hidden",
    justifyContent: "center",
    flexDirection: "row"
  },
  text: {
    fontFamily: family.medium,
    ...sizes.button
  },
  loading: {
    marginLeft: 5
  }
});

Button.defaultProps = {
  container: {},
  style: {},
  isLoading: false
};

export default memo(Button);
