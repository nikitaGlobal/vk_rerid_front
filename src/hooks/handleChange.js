import { useState, useEffect } from "react";

export default function useHandleChange(initialValue) {
  const [value, setValue] = useState(initialValue);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    setValue(initialValue);
    setSuccess(false);
    setError(false);
  }, [initialValue]);

  function verifyEmail(data, errMessage) {
    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(data)) {
      setErrorMessage("");
      return true;
    }
    setErrorMessage(errMessage);
    return false;
  }
  function verifyLength(targetValue, length, errMessage) {
    if (targetValue?.length >= length) {
      setErrorMessage("");
      return true;
    }

    setErrorMessage(errMessage);
    return false;
  }
  function verifyNumber(targetValue, errMessage) {
    const numberRex = new RegExp("^[0-9]+$");
    if (numberRex.test(targetValue)) {
      setErrorMessage("");
      return true;
    }

    setErrorMessage(errMessage);
    return false;
  }
  function verifyUrl(value) {
    try {
      // eslint-disable-next-line no-undef
      new URL(value);
      return true;
    } catch (_) {
      return false;
    }
  }

  function verifyEqual(a, b, errMessage) {
    if (a !== b && errMessage) {
      setErrorMessage(errMessage);
    } else {
      setErrorMessage("");
    }
    return a === b;
  }

  function validate({ targetValue, type, length, secondValue, errMessage }) {
    let isValid = true;

    switch (type) {
      case "email":
        isValid = verifyEmail(
          targetValue,
          "Пожалуйста, введите действительный адрес электронной почты" ||
            errMessage
        );
        break;
      case "password":
        isValid = verifyLength(
          targetValue,
          8,
          "Пароль должен содержать не менее 8 символов" || errMessage
        );
        break;
      case "checkbox":
        isValid = targetValue.checked;
        break;
      case "number":
        isValid = verifyNumber(
          targetValue,
          "Пожалуйста, используйте только цифры" || errMessage
        );
        break;
      case "max-length":
        isValid = !verifyLength(targetValue, length + 1, errMessage);
        break;
      case "url":
        isValid = verifyUrl(targetValue, errMessage);
        break;
      case "min-length":
        isValid = verifyLength(targetValue, length, errMessage);
        break;
      case "equal":
        isValid = verifyEqual(targetValue, secondValue, errMessage);
        break;
      default:
        break;
    }

    setError(!isValid);
    return isValid;
  }

  function handleChange({
    targetValue,
    type,
    length,
    secondValue,
    errMessage
  }) {
    setValue(targetValue);
    return validate({
      targetValue,
      type,
      length,
      secondValue,
      errMessage
    });
  }

  return {
    value,
    success,
    error,
    errorMessage,
    validate,
    onChange: handleChange
  };
}
